#cmake_minimum_required (VERSION 3.1)

#set (IS_CROSSCOMPILING "YES")

SET(CMAKE_SYSTEM_NAME Windows)

SET(CMAKE_C_COMPILER "x86_64-w64-mingw32-gcc")
SET(CMAKE_CXX_COMPILER "x86_64-w64-mingw32-g++")
SET(CMAKE_RC_COMPILER "x86_64-w64-mingw32-windres")
#.txtSET(CMAKE_FIND_ROOT_PATH  "/usr/local/Cellar/mingw-w64/6.0.0_2")
SET(CMAKE_FIND_ROOT_PATH  "/usr/local/Cellar/mingw-w64/6.0.0_2/toolchain-i686/i686-w64-mingw32")
#SET(CMAKE_EXE_LINKER_FLAGS  "-lmsvcrt -mconsole -lmingw32 -municode")
#SET(CMAKE_EXE_LINKER_FLAGS  "-municode")


# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)


link_libraries(-static-libgcc -static-libstdc++)

