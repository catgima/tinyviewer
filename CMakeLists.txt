cmake_minimum_required(VERSION 3.0)

set(CMAKE_VERBOSE_MAKEFILE OFF)


include(cmake_scripts/CleanBuildCacheFile.cmake)

find_package(Git REQUIRED)
if(Git_FOUND)
  message("Git found: ${GIT_EXECUTABLE}")
endif()

list(APPEND CMAKE_MODULE_PATH cmake_scripts/CMakePCHCompiler)
project(ftools C CXX CXXPCH)

set (pch_header_name pch.h)

#-------------------------------------------------------------------------------
message ("CMake version is ${CMAKE_VERSION}")
message ("CMAKE_SYSTEM_NAME: ${CMAKE_SYSTEM_NAME}")
message ("PROJECT_SOURCE_DIR: ${PROJECT_SOURCE_DIR}")
message ("PROJECT_BINARY_DIR: ${PROJECT_BINARY_DIR}")
message ("CMAKE_CURRENT_SOURCE_DIR: ${CMAKE_CURRENT_SOURCE_DIR}")
message ("CMAKE_MAKE_PROGRAM: ${CMAKE_MAKE_PROGRAM}")
message ("CMAKE_CXX_COMPILER_ID: ${CMAKE_CXX_COMPILER_ID}")
message ("CMAKE_SYSTEM_PROCESSOR: ${CMAKE_SYSTEM_PROCESSOR}")
message ("CMAKE_SYSTEM_VERSION: ${CMAKE_SYSTEM_VERSION}")
message ("CMAKE_HOST_SYSTEM_NAME : ${CMAKE_HOST_SYSTEM_NAME}")


#compiler flag setting
if(CMAKE_COMPILER_IS_GNUCXX OR ${CMAKE_SYSTEM_NAME} STREQUAL Darwin OR ${CMAKE_CXX_COMPILER_ID} STREQUAL Clang)
    execute_process(COMMAND ${CMAKE_C_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)
    
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -fshort-wchar")
    if(${CMAKE_SYSTEM_NAME} STREQUAL Linux)
        SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-reorder")
    elseif(${CMAKE_SYSTEM_NAME} STREQUAL Windows)
    set (CMAKE_SYSTEM_PROCESSOR "x86_64")
    endif()
    
    add_definitions("-Wno-unused-variable")
    add_definitions("-Wno-unused-result")
    add_definitions("-Wno-missing-braces")
    add_definitions("-Wno-unused-label")
    add_definitions("-Wno-deprecated-declarations")
    
    if(MINGW)
        SET(CMAKE_EXE_LINKER_FLAGS  "-municode")
        #add_definitions("-Wno-write-strings")
        #SET (EXT_FLAG "${EXT_FLAG} -fPIC") 
        #SET (EXT_FLAG "${EXT_FLAG} -fno-stack-protector") 
    else()
        SET (EXT_FLAG "${EXT_FLAG} -fPIC") 
        add_definitions("-Wno-pointer-bool-conversion")
        add_definitions("-Wno-implicit-exception-spec-mismatch")

        if(${CMAKE_BUILD_TYPE} STREQUAL Debug)
            #SET (EXT_FLAG "${EXT_FLAG} -fstandalone-debug")
            #SET (EXT_FLAG "${EXT_FLAG} -fsanitize=address -fno-omit-frame-pointer -fsanitize=leak") 
        endif()
    endif()

    SET (EXT_FLAG "${EXT_FLAG} -m64 -Winvalid-pch") 
    SET (EXT_FLAG "${EXT_FLAG} -D_FILE_OFFSET_BITS=64") 
    SET (EXT_FLAG "${EXT_FLAG} -DLARGEFILE_SOURCE ") 
    SET (EXT_FLAG "${EXT_FLAG} -DLARGEFILE64_SOURCE") 
    SET (EXT_FLAG "${EXT_FLAG} -D__STDC_FORMAT_MACROS") 

    set (CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O0 -g -ggdb ${EXT_FLAG}")
    set (CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -O3 ${EXT_FLAG}")

    set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -O0 -g -ggdb -gdwarf-3 ${EXT_FLAG}")
    set (CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O3 ${EXT_FLAG}")
endif()

if(NOT ${CMAKE_SYSTEM_NAME} STREQUAL Darwin)
    if(NOT ${CMAKE_BUILD_TYPE} STREQUAL Debug)
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
        link_libraries(-static-libgcc -static-libstdc++) 
        message ("set static link")    
    else()
        message ("set dynamic link")    
    endif()
else()
    set(CMAKE_EXE_LINKER_FLAGS "-framework Cocoa -framework OpenGL -framework IOKit")
    set(OPENSSL_ROOT_DIR "/opt/homebrew/opt/openssl")
endif()    

if (${CMAKE_SYSTEM_PROCESSOR} STREQUAL arm64)
    set(CMAKE_OSX_ARCHITECTURES "arm64" CACHE STRING "")
elseif(${CMAKE_SYSTEM_PROCESSOR} STREQUAL AMD64)
    set(CMAKE_OSX_ARCHITECTURES "x86_64" CACHE STRING "")
endif()

file(GLOB nedmalloc_file ${CMAKE_CURRENT_SOURCE_DIR}/src/nedmalloc/*.c)
list(FILTER nedmalloc_file EXCLUDE REGEX "._")

file(GLOB yyjson_file ${CMAKE_CURRENT_SOURCE_DIR}/src/yyjson/*.c)
list(FILTER yyjson_file EXCLUDE REGEX "._") 

file(GLOB_RECURSE CppFiles RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "*.cpp")
list(FILTER CppFiles EXCLUDE REGEX "._") 
list(FILTER CppFiles EXCLUDE REGEX "CMakeFiles") 
list(FILTER CppFiles EXCLUDE REGEX "main.cpp") 
list(FILTER CppFiles EXCLUDE REGEX "CMakePCHCompiler")
list(FILTER CppFiles EXCLUDE REGEX "mingwbuild")
list(FILTER CppFiles EXCLUDE REGEX "osxbuild")
list(FILTER CppFiles EXCLUDE REGEX "linuxbuild")


#FOREACH (name ${nedmalloc_file})
#    message ("---${name}")
#ENDFOREACH ()

FOREACH (name ${CppFiles})
    message ("---${name}")
ENDFOREACH ()

SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib)
set(CMAKE_HEADER_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/include)
file(MAKE_DIRECTORY ${CMAKE_HEADER_OUTPUT_DIRECTORY})

find_package(Threads REQUIRED)

#---- find openssl library
message("---------------------- OPENSSL check ---------------------")
if(WIN32)
    set(OPENSSL_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/dependLibrary/include")
    set(OPENSSL_LIBRARIES 
        "${PROJECT_SOURCE_DIR}/dependLibrary/lib/mingw64/libssl.a"
        "${PROJECT_SOURCE_DIR}/dependLibrary/lib/mingw64/libcrypto.a"
    )
else()
    find_package(OpenSSL REQUIRED)
    if(NOT OpenSSL_FOUND)
        message("OPENSSL not found")
        #add_subdirectory(cmake_scripts/openssl-cmake)
    endif()
endif()    

message("OPENSSL_ROOT_DIR: ${OPENSSL_ROOT_DIR}")
message("OPENSSL INC PATH: ${OPENSSL_INCLUDE_DIR}")
message("OPENSSL LIB PATH: ${OPENSSL_LIBRARIES}") 


INCLUDE_DIRECTORIES(
    ${PROJECT_SOURCE_DIR}
    ${PROJECT_SOURCE_DIR}/src/nedmalloc
    ${PROJECT_SOURCE_DIR}/src/yyjson
    ${PROJECT_SOURCE_DIR}/src/utility
    ${PROJECT_SOURCE_DIR}/src/thread-pool
    ${PROJECT_SOURCE_DIR}/src
    ${CMAKE_HEADER_OUTPUT_DIRECTORY}/include
    ${OPENSSL_INCLUDE_DIR}
)

# lib directories
LINK_DIRECTORIES(
    ${PROJECT_BINARY_DIR}/lib
    ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}
)

message("INCLUDE_DIRECTORIES: ${INCLUDE_DIRECTORIES}")
message("LINK_DIRECTORIES: ${LINK_DIRECTORIES}")

add_library(utility STATIC ${CppFiles})
add_library(nedmalloc STATIC ${nedmalloc_file})
add_library(yyjson STATIC ${yyjson_file})

add_executable(${PROJECT_NAME} ./src/main.cpp)  
target_link_libraries(${PROJECT_NAME} utility)
target_link_libraries(${PROJECT_NAME} nedmalloc)
target_link_libraries(${PROJECT_NAME} yyjson)
target_link_libraries(${PROJECT_NAME} pthread)
target_link_libraries(${PROJECT_NAME} ${LINK_DIRECTORIES} ${OPENSSL_LIBRARIES})

target_precompiled_header(utility ${pch_header_name})
target_precompiled_header(${PROJECT_NAME} ${pch_header_name} REUSE utility)

if(${CMAKE_SYSTEM_NAME} STREQUAL Darwin)
    add_custom_command(
        TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND lipo -info ${PROJECT_BINARY_DIR}/bin/${PROJECT_NAME} 
    )
endif()
 