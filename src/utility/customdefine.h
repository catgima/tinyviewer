#pragma once

#ifdef __APPLE__
	#include <inttypes.h>
#else
	#ifdef __MINGW32__
		#ifndef int64_t
			#define int64_t __int64
		#endif 

		#ifdef uint64_t 
			#undef uint64_t
			#define uint64_t long long unsigned int
		#else	
			#define uint64_t long long unsigned int
		#endif 
	#endif
#endif

#if defined(__MINGW32__)
	#define CChar wchar_t
	#define CStdString std::wstring
	#define CStrlen wcslen
	#define CStrcpy wcscpy
	#define CChdir _wchdir
	#define Cvsprintf vswprintf
	#define GetCurrentDir _wgetcwd
#else
	#define CChar char
	#define CStdString std::string
	#define CChdir chdir
	#define CStrcpy strcpy
	#define CStrlen strlen
	#define Cvsprintf vsnprintf
	#define GetCurrentDir getcwd
	typedef unsigned long DWORD;
	typedef unsigned long long ULONGLONG;
#endif

#if defined(__MINGW32__)
	#define  FNM_PATHNAME  (1 << 0) /* No wildcard can ever match `/'. */
	#define  FNM_NOESCAPE  (1 << 1) /* Backslashes don't quote special chars. */
	#define  FNM_PERIOD    (1 << 2) /* Leading `.' is matched only explicitly. */
	#define  FNM_NOMATCH    1
	
	int fnmatch_win(const CChar *pattern, const CChar *name, int flags = 0);
	#define fnmatch fnmatch_win
#endif

#ifndef _T
	#define _T(x) L ##x
	#else
	#define _T(x) x
#endif


void GetWorkPath(CChar* _buf, int _length);
void ToLower(CChar *src);
void RStripString(CChar *fname, CChar _symbo);
std::string StrW2S (std::wstring src);
std::wstring StrS2W (std::string src);

int Csprintf (CChar* sDest, size_t len, const CChar* sFormat, ...);

template <typename T> static std::vector <T>& StrSplit (T inStr, T sepStr, std::vector<T>& outStrVec)  {
	size_t token = 0;
	typename T::size_type found = inStr.find_first_of (sepStr);
	while (found != T::npos) {
		outStrVec.push_back (inStr.substr (token, found - token));
		token = found + 1;
		found = inStr.find_first_of (sepStr, token);
	}
	T str = inStr.substr (token, inStr.size());
	if (str.size() > 0) {
		outStrVec.push_back (inStr.substr (token, inStr.size()));
	}
	return outStrVec;
}

template <typename T> static void ReplaceKeyChar(T oldKey, T newKey, T* SrcStr) {
	size_t i = 0;
	while (SrcStr[i]){
		if (SrcStr[i] == oldKey)
			SrcStr[i] = newKey;
		i++;
	}
}

template <typename T> static T RemoveStringSpace (T &s) 
{
	if (s.empty()) {
	    return s;
	}
	s.erase (0, s.find_first_not_of(_T(" ")));
	s.erase (s.find_last_not_of(_T(" ")) + 1);
	return s;
}

template<class T> static void DeleteMapContent (T& _kMap) {
	for (typename T::iterator it=_kMap.begin(); it!=_kMap.end(); ++it) {
		delete(it->second);
	}
	_kMap.clear();
}

template<class T> static void DeleteVectorContent (T& _kVector) {
	for (typename T::iterator it=_kVector.begin(); it!=_kVector.end(); ++it) {
		delete(*it);
	}
	_kVector.clear();
}

template<class T> static void DeleteQueueContent (T& _kQueue) {
	while( _kQueue.size() > 0 ) {
		delete (_kQueue.front());
		_kQueue.pop();
	}
}

template<class T> static void DeleteListContent (T& _kList) {
	while( _kList.size() > 0 ) {
		delete (_kList.back());
		_kList.pop_back();
	}
}

template<class T >
typename T::mapped_type FindMap( T* _pkMap, typename T::key_type _key )
{
    typename T::const_iterator it = _pkMap->find( _key );
    return ( it!=_pkMap->end() ) ? it->second :NULL;
}




