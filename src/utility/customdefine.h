#pragma once

#include "nedmalloc.h"
#include "Logger.h"

#ifdef __APPLE__
	#include <inttypes.h>
#else
	#ifdef __MINGW32__
		#ifndef int64_t
			#define int64_t __int64
		#endif 

		#ifdef uint64_t 
			#undef uint64_t
			#define uint64_t long long unsigned int
		#else	
			#define uint64_t long long unsigned int
		#endif 
	#endif
#endif

/*
void *operator new(size_t size) {
	return malloc(size);
}

void operator delete(void *p) {
	free(p);
}

void * operator new [](size_t size) {
	return operator new(size);
}

void operator delete[](void * pointer) {
	operator delete(pointer);
}*/

#if defined(__MINGW32__)
	#define CStdString std::wstring
	#define CStdRegex std::wregex
	#define CStdSmatch std::wsmatch
	#define CStdSub_match std::wcsub_match
	#define CStdRegex_search std::wregex_search
	#define CStdCout std::wcout

	#define CChar wchar_t
	#define CStrlen wcslen
	#define CStrcpy wcscpy
	#define CStrncpy wcsncpy
	#define CChdir _wchdir
	#define Cvsprintf vswprintf
	#define GetCurrentDir _wgetcwd
	#define CPrintf wprintf
	#define CToLower towlower
#else
	#define CStdString std::string
	#define CStdRegex std::regex
	#define CStdSmatch std::smatch
	#define CStdSub_match std::ssub_match
	#define CStdRegex_search std::regex_search
	#define CStdCout std::cout

	#define CChar char
	#define CChdir chdir
	#define CStrcpy strcpy
	#define CStrncpy strncpy
	#define CStrlen strlen
	#define Cvsprintf vsnprintf
	#define GetCurrentDir getcwd
	#define CPrintf printf
	#define CToLower tolower

	typedef unsigned long DWORD;
	typedef unsigned long long ULONGLONG;
#endif

#ifndef _T
	#if defined(__MINGW32__)
		#define _T(x) L ##x
	#else
		#define _T(x) x
	#endif
#else			
	#define _T(x) x
#endif

#if defined(_WIN32)
#define ALIGNED(alignsize) __declspec(align(alignsize))
#else
#define ALIGNED(alignsize) __attribute__((aligned(alignsize)))
#endif

//#pragma GCC diagnostic warning "-Wformat"

////////////////////// template ////////////////////////////

template <class T> class Singleton 
{
public:
	static T& instance() {
		static T _instance;
		return _instance;
	}
protected:
	Singleton () {}
	virtual ~Singleton() {}
	Singleton (const Singleton<T>&);
	Singleton<T>& operator= (const Singleton<T> &);
};

template<class T> static void DeleteMapContent (T& _kMap) {
	for (typename T::iterator it=_kMap.begin(); it!=_kMap.end(); ++it) {
		delete(it->second);
	}
	_kMap.clear();
}

template<class T> static void DeleteVectorContent (T& _kVector) {
	for (typename T::iterator it=_kVector.begin(); it!=_kVector.end(); ++it) {
		delete(*it);
	}
	_kVector.clear();
}

template<class T> static void DeleteQueueContent (T& _kQueue) {
	while( _kQueue.size() > 0 ) {
		delete (_kQueue.front());
		_kQueue.pop();
	}
}

template<class T> static void DeleteListContent (T& _kList) {
	while( _kList.size() > 0 ) {
		delete (_kList.back());
		_kList.pop_back();
	}
}

template<class T > typename T::mapped_type FindMap( T* _pkMap, typename T::key_type _key )
{
    typename T::const_iterator it = _pkMap->find( _key );
    return ( it!=_pkMap->end() ) ? it->second :NULL;
}

template<class T> static inline unsigned int HashKey(T key, bool fastkey = false) 
{
	unsigned int nHash = 2166136261UL;
	if (!fastkey) {
		while(*key)	{
			nHash = (nHash * 16777619UL) ^ *key++;
		}
		return nHash;
	}
	nHash = 0;
	while (*key)
		nHash = (nHash<<5) + nHash + *key++;
	return nHash;
}

#if __MINGW32__
	std::wstring CHARtoWCHAR (const std::string& wstr, UINT codePage = 65001);
	std::string WCHARtoCHAR (const std::wstring& wstr, UINT codePage = CP_UTF8);
#endif

