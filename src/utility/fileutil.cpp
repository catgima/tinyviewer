#include "pch.h"
#include <assert.h>

#ifdef __GNUC__
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <dirent.h>
	#include <errno.h>
	#include <stdarg.h>
#endif

#ifdef __MINGW32__
	#include <iphlpapi.h>
	#include <share.h>
#endif

#include "openssl/md5.h"

#if !defined(__GNUC__)
	#pragma comment(lib,"Iphlpapi.lib")
	#pragma comment(lib,"libeay32")
#endif

#include "ByteStream.h"
#include "fileutil.h"

bool FileUtil::CreateDir (const CChar* Path)
{
#if defined(__MINGW32__)
	_SECURITY_ATTRIBUTES attrib = {0};
	if (CreateDirectoryW (Path, &attrib) == 0)
		return false;
	ChangeAttrib (Path, FILE_ATTRIBUTE_NORMAL);
	return true;
#else
	int status = mkdir (Path, 0777);
	if (status != -1) {
		chmod (Path, 0777);
		return true;
	}
#endif
	return false;
}

bool FileUtil::IsDirExist (const CChar* nPathString)
{
#if defined(_WIN32) 
	DWORD ftyp = GetFileAttributesW (nPathString);
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;

	return false;
#else
	DIR* dir = opendir (nPathString);
	if (dir){
		closedir(dir);
		return true;
	}
	else if (errno == ENOENT)	//This directory does not exist.
		return false;
	else if (errno == ENOTDIR)	//This file is not a directory.
		return false;
	return false;
#endif
}

bool FileUtil::IsFileExist (const CChar* Name)
{
#if defined(__MINGW32__)
	struct _stat64 info;
	int Result = _wstat64 (Name, &info);
#else
	struct stat info;
	int Result = stat (Name, &info);
#endif

	return Result == 0;
}

#if defined(__MINGW32__)
DWORD FileUtil::ChangeAttrib (const wchar_t* PathName, DWORD AttrValue)
{
	DWORD err = 0;
	DWORD dwFileAttributes = FILE_ATTRIBUTE_ARCHIVE;

	_WIN32_FIND_DATAW FindFileData;
	HANDLE hFind = FindFirstFileW (PathName, &FindFileData);
	if(hFind == INVALID_HANDLE_VALUE) {
		err = GetLastError();
		FindClose(hFind);
		return err;
	}
	FindFileData.dwFileAttributes = AttrValue;
	FindClose(hFind);
	if (!SetFileAttributesW (PathName, FILE_ATTRIBUTE_ARCHIVE)) {
		return GetLastError();
	}
	return 0;
}
#endif

bool FileUtil::SaveFile(const CChar* filePath, CStdString& buffer)
{
	FILE* fp;
	if (!Cfopen (&fp, filePath, _T("wt, ccs=UTF-8"))){
		return false;
	}
#ifndef __MINGW32__    
    flockfile (fp);
#endif
    fwrite (&buffer[0], sizeof(CChar), buffer.size(), fp);
	fclose (fp);
#ifndef __MINGW32__
    funlockfile (fp);
#endif
    fclose (fp);
    return true;
}

bool FileUtil::OpenFile(const CChar* filePath, std::string& buffer)
{
	FILE* fp;
	if (!Cfopen (&fp, filePath, _T("rb"))) {
		return false;
	}
#ifndef __MINGW32__    
    flockfile (fp);
#endif
    fseek (fp, 0, SEEK_END);
	unsigned int nfileSize = (unsigned int)ftell (fp);
	fseek (fp, 0, SEEK_SET);

	buffer.resize (nfileSize);
	fread (&buffer[0], nfileSize, 1, fp);

#ifndef __MINGW32__
    funlockfile (fp);
#endif
    fclose (fp);
    return true;
}

void FileUtil::StreamWriteFile (ByteStream* streamdata, const CChar* FileName)
{
	FILE* fp;
	if (Cfopen (&fp, FileName, _T("wb"))) {
#ifndef __MINGW32__
        flockfile (fp);
#endif
		fwrite (&streamdata->Packet[0], streamdata->Packet.size(), 1, fp);
#ifndef __MINGW32__
        funlockfile (fp);
#endif
		fclose (fp);
	}
}

void FileUtil::StreamLoadFile (ByteStream* streamdata, const CChar* FileName)
{
	FILE* fp;
	if (!Cfopen (&fp, FileName, _T("rb")))
		return;
#ifndef __MINGW32__
    flockfile (fp);
#endif
	fseek (fp, 0, SEEK_END);
	unsigned int nFileSize = (unsigned int)ftell (fp);
	fseek (fp, 0, SEEK_SET);

	streamdata->Packet.resize (nFileSize);
	fread (&streamdata->Packet[0], nFileSize, 1, fp);
#ifndef __MINGW32__
    funlockfile (fp);
#endif    
	fclose (fp);
}

bool FileUtil::Cfopen (FILE** fileptr, const CChar* filename, const CChar* mode)
{
#if !defined(__MINGW32__)	
	return (*fileptr = fopen (filename, mode)) ? true : false;
#else
	return (*fileptr = _wfsopen (filename, mode, _SH_DENYNO)) ? true : false ;	
#endif	
}

int FileUtil::Csprintf (CChar* sDest, size_t len, const CChar* sFormat, ...) 
{
	va_list argList;
	va_start(argList, sFormat);
	size_t nCount = Cvsprintf(sDest, len, sFormat, argList);
	va_end(argList);
	assert (nCount<len && "Csprintf buffer overflow!");
	return (int)nCount;
}

void FileUtil::StreamMakeMd5 (FILE* fp, unsigned char* buffer)
{
	MD5_CTX mdContext;
	unsigned char data[1024];

	fseek (fp, 0, SEEK_END);
	unsigned int TotolSize = (unsigned int)ftell (fp);
	fseek (fp, 0, SEEK_SET);

	MD5_Init (&mdContext);

	int Readloop = 1;
	int ReadSize;
	while (Readloop) 
	{
		if (TotolSize < 1024) {
			ReadSize = (int)TotolSize;
			memset (&data, 0, 1024);
		}
		else {
			ReadSize = 1024;
		}

		if (fread (data, ReadSize, 1, fp) == 0)
			break;

		TotolSize -= ReadSize;
		MD5_Update (&mdContext, data, ReadSize);
	}

	MD5_Final (buffer, &mdContext);
}

CStdString FileUtil::MakeMd5Checksum (CStdString filename)
{
	FILE* inFile;
	if (!Cfopen (&inFile, filename.c_str(), _T("rb")))
		return _T("");

#ifndef _WIN32
    flockfile (inFile);
#endif

	unsigned char buffer [MD5_DIGEST_LENGTH] = {0};
	StreamMakeMd5 (inFile, buffer);

#ifndef _WIN32
    funlockfile (inFile);
#endif
	fclose (inFile);

	CChar checksum [128] = {0};
	for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
		Csprintf (checksum, 128, _T("%s%02x"), checksum, buffer[i]);
	}
	return checksum;
}

CStdString FileUtil::MakeMd5Checksum (unsigned char* buffer, size_t len)
{
	unsigned char tarBuffer [MD5_DIGEST_LENGTH] = {0};
	
	MD5_CTX context;
	MD5_Init (&context);
	MD5_Update (&context, buffer, len);
	MD5_Final (tarBuffer, &context);

	CChar checksum[128] = {0};
	for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
		Csprintf(checksum, 128, _T("%s%02x"), checksum, tarBuffer[i]);
	}

	return checksum;
}
