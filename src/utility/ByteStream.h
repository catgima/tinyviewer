#pragma once

#define StreamBufSize 8192

class ByteStream : public ObjectBase
{
public:
	ByteStream() {}
	~ByteStream() {}

	inline void operator << (wchar_t* arg) {
		*this << std::wstring (arg);
	}

	inline void operator << (char* arg) {
		*this << std::string (arg);
	}

	inline void operator << (std::string arg) {
		SetStdArrayContainer <std::string> (arg);
	}

	inline void operator << (std::wstring arg) {
		SetStdArrayContainer <std::wstring> (arg);
	}
    
	inline void operator << (int arg) {
		SetFixedByte <int, sizeof(int)> (arg);
	}

#ifdef __APPLE_CC__
    inline void operator << (unsigned long arg) {
        unsigned int newarg = (unsigned int)arg;
        SetFixedByte <unsigned int, sizeof(unsigned int)> (newarg);
    }
#endif
    
	inline void operator << (unsigned int arg) {
		SetFixedByte <int, sizeof(int)> (arg);
	}

	inline void operator << (int64_t arg) {
		SetFixedByte <int64_t, sizeof(int64_t)> (arg);
	}

	inline void operator << (float arg) {
		SetFixedByte <float, sizeof(float)> (arg);
	}

	inline void operator << (double arg) {
		SetFixedByte <double, sizeof(double)> (arg);
	}

	inline void operator << (short arg) {
		SetFixedByte <short, sizeof(short)> (arg);
	}

	inline void operator << (bool arg) {
		SetFixedByte <bool, sizeof(bool)> (arg);
	}

	inline void operator << (unsigned short arg) {
		SetFixedByte <unsigned short, sizeof(unsigned short)> (arg);
	}

	inline void operator << (char arg) {
		SetFixedByte <char, sizeof(char)> (arg);
	}

	inline void operator << (wchar_t arg) {
		SetFixedByte <wchar_t, sizeof(wchar_t)> (arg);
	}

	inline void operator << (unsigned char arg) {
		SetFixedByte <unsigned char, sizeof(unsigned char)> (arg);
	}

	template <typename T> inline void operator << (std::vector <T>& arg) {
		SetStdArrayContainer (arg);
	}

	template <typename T> inline void operator << (std::list <T>& arg) {
		SetStdArrayContainer (arg);
	}

//////////////////////////////////////////////////

	inline void operator >> (std::wstring& arg) {
		GetStdArrayContainer (arg, sizeof(wchar_t));
	}

	inline void operator >> (std::string& arg) {
		GetStdArrayContainer (arg, sizeof(char));
	}

	inline void operator >> (wchar_t* arg) {
		GetCharArray (arg, sizeof(wchar_t));
	}

	inline void operator >> (char* arg) {
		GetCharArray (arg, sizeof(char));
	}

	inline void operator >> (float& arg) {
		GetFixedByte <float, sizeof(float)> (arg);
	}

	inline void operator >> (double& arg) {
		GetFixedByte <double, sizeof(double)> (arg);
	}

    inline void operator >> (int& arg) {
		GetFixedByte <int, sizeof(int)> (arg);
	}
    
    inline void operator >> (int64_t& arg) {
		GetFixedByte <int64_t, sizeof(int64_t)> (arg);
	}

#ifdef __APPLE_CC__
    inline void operator >> (size_t& arg) {
        unsigned int newarg = 0;
		GetFixedByte <unsigned int, sizeof(unsigned int)> (newarg);
        arg = (size_t)newarg;
	}
#endif
    
	inline void operator >> (unsigned int& arg) {
		GetFixedByte <unsigned int, sizeof(unsigned int)> (arg);
	}
    
	inline void operator >> (short& arg) {
		GetFixedByte <short, sizeof(short)> (arg);
	}

	inline void operator >> (bool& arg) {
		GetFixedByte <bool, sizeof(bool)> (arg);
	}

	inline void operator >> (unsigned short& arg) {
		GetFixedByte <unsigned short, sizeof(unsigned short)> (arg);
	}

	inline void operator >> (char& arg) {
		GetFixedByte <char, sizeof(char)> (arg);
	}

	inline void operator >> (wchar_t& arg) {
		GetFixedByte <wchar_t, sizeof(wchar_t)> (arg);
	}

	inline void operator >> (unsigned char& arg) {
		GetFixedByte <unsigned char, sizeof(unsigned char)> (arg);
	}

	template <typename T> inline void operator >> (std::vector <T>& arg) {
		GetStdArrayContainer (arg, sizeof(T));
	}

	template <typename T> inline void operator >> (std::list <T>& arg) {
		GetStdArrayContainer (arg, sizeof(T));
	}

	template <typename T, size_t Count> void InsertFixedByte (T TypeArg) {
		std::vector<unsigned char> buffer (Count+Packet.size());
		memcpy (&buffer[0], &TypeArg, Count);
		memcpy (&buffer[Count], &Packet[0], Packet.size());
		Packet.swap (buffer);
	}

	template <typename T, size_t Count> void SetFixedByte (T TypeArg) {
		size_t oldsize = Packet.size();
		Packet.resize (Count+Packet.size());
		memcpy (&Packet[oldsize], &TypeArg, Count);
	}

	void GetFixedByte (std::vector<unsigned char>& TypeArg, size_t Count) {
		TypeArg.resize (Count);
		memcpy (&TypeArg[0], &Packet[0], Count);
		Packet.erase (Packet.begin(), Packet.begin()+Count);
	}
    
    void PutBinary (const char* binBuffer, unsigned int bufferLengt) {
        if ( bufferLengt > 0) {
            size_t oldsize = Packet.size();
            Packet.resize (bufferLengt+Packet.size());
            memcpy (&Packet[oldsize], &binBuffer[0], bufferLengt);
        }
    }

    void GetBinary (char * binBuffer, unsigned int bufferLengt) {
        if (Packet.size() >= bufferLengt ) {
            memcpy (&binBuffer[0], &Packet[0], bufferLengt);
            Packet.erase(Packet.begin(), Packet.begin()+bufferLengt);
        }
    }

	template <typename T> inline void ReplaceSpecificByte (T TypeArg, size_t nStartPos, size_t nLength) {
		memcpy (&Packet[0+nStartPos], &TypeArg, nLength);
	}

	inline void RemoveSpecificByte (size_t length) {
		Packet.erase (Packet.begin(), Packet.begin()+length);
	}

protected:
	template <typename T, size_t Count> inline void GetFixedByte(T& TypeArg) {
		if (Packet.size() > 0) {
			memcpy (&TypeArg, &Packet[0], Count);
			Packet.erase (Packet.begin(), Packet.begin()+Count);
		}
		else TypeArg = 0;
	}

	template <typename T> void SetStdArrayContainer(T& arg)
    {
        short sz = (short)arg.size();
		*this << sz;
		if (arg.size() == 0)
			return;

		size_t oldsize = Packet.size();
		size_t elementsize = sizeof(arg[0]);
		Packet.resize (Packet.size()+arg.size()*elementsize);
		memcpy (&Packet[oldsize], &arg[0], arg.size()*elementsize);
	}

	template <typename T> void GetStdArrayContainer (T& arg, size_t elementSize) {
		short size;
        *this >> size;		
		arg.resize(size);
		if (size == 0) {
			return;
		}
		size_t ByteArraySize = elementSize*size;
		if (Packet.size() < ByteArraySize) {
			ByteArraySize = Packet.size();
		}

		memcpy (&arg[0], &Packet[0], ByteArraySize);
		Packet.erase (Packet.begin(), Packet.begin()+ByteArraySize);
	}

	template <typename T> void GetCharArray (T* arg, size_t charsize) {
		short size;
        *this >> size;
		if (size == 0)
			return;
		size_t ByteArraySize = size*charsize;
		memcpy (arg, &Packet[0], ByteArraySize);
		Packet.erase (Packet.begin(), Packet.begin()+ByteArraySize);
	}

public:
	std::vector <unsigned char> Packet;
};


