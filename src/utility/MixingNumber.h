#pragma once

class SMixingNumber : public ObjectBase
{
	union UMixingNumber {
		ULONGLONG number;
		DWORD CombinNumber[2];
	};
	UMixingNumber m_uNumberinfo;

public:
	SMixingNumber();
	SMixingNumber(SMixingNumber& _otherObj);
	SMixingNumber(DWORD _low, DWORD _hi);
	SMixingNumber(uint64_t _number);

#if defined(__MINGW32__)
	SMixingNumber(_FILETIME _time);
#endif

	inline uint64_t GetNum() const {
		return m_uNumberinfo.number;
	}
	inline DWORD GetHi() const {
		return m_uNumberinfo.CombinNumber[1];
	}
	inline DWORD GetLow() const {
		return m_uNumberinfo.CombinNumber[0];
	}

	SMixingNumber& operator=(const SMixingNumber& _otherObj);
};