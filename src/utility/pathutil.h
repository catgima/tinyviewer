#pragma once

#ifndef _MAX_PATH
	#define _MAX_PATH   260 			/* max. length of full pathname */
#endif //_MAX_PATH

#ifndef _MAX_DRIVE
	#define _MAX_DRIVE  3   			/* max. length of drive component */
#endif //_MAX_DRIVE

#ifndef _MAX_DIR
	#define _MAX_DIR    256 			/* max. length of path component */
#endif //_MAX_DIR

#ifndef _MAX_FNAME
	#define _MAX_FNAME   260 			/* max. length of file name component */
#endif //_MAX_FNAME

#ifndef _MAX_EXT
	#define _MAX_EXT    256 			/* max. length of extension component */
#endif //_MAX_EXT

struct SPathFileInfo
{
	bool IsDir;
	CChar drv [_MAX_DRIVE];
	CChar dir [_MAX_DIR];
	CChar file [_MAX_FNAME];
	CChar ext [_MAX_EXT];
};

class PathUtil : public Singleton<PathUtil>, ObjectBase
{
public:
	PathUtil() {}
	~PathUtil() {}

	void SplitPath(const CChar* pcFullPath, SPathFileInfo& _info);
	CStdString GetWorkPath();
	bool IsPathIsDir(const CChar* _PathName);
	CStdString getUserDir();
	CStdString GetCurrentFullPath();
};