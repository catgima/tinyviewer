#pragma once

struct SRes {
	double cpu;
	double prog;
};

class SystemInfo : public Singleton<SystemInfo>, ObjectBase
{
public: 
	int StringMaxLength;

public:	
	SystemInfo();
	~SystemInfo();

	void Init();
	void GetSystemBaseInfo();
	unsigned long long getTotalSystemMemory();
};
