#pragma once

union SBitFieldAttrib
{
	unsigned short attrib_info; 
	struct {
		unsigned short is_file : 1;
		unsigned short is_system : 1;
		unsigned short is_link : 1;
		unsigned short is_hidden : 1;
		unsigned short is_dir : 1;

		//file permission format rwxrwxrwx
		unsigned short is_u_read : 1;
		unsigned short is_u_write : 1;
		unsigned short is_u_exec : 1;
		unsigned short is_g_read : 1;
		unsigned short is_g_write : 1;
		unsigned short is_g_exec : 1;
		unsigned short is_o_read : 1;
		unsigned short is_o_write : 1;
		unsigned short is_o_exec : 1;
	};
};

struct SFileInfoBase
{
	SMixingNumber time_create;  
    SMixingNumber time_access;  
    SMixingNumber time_write;   
	SMixingNumber file_size;
	SBitFieldAttrib file_attrib;
	CStdString file_name;
	CStdString symlink_target;
};

struct dirent;
class CfileInfo 
{
	SFileInfoBase m_pkInfo;

public:
	CfileInfo();
	
#if defined(__MINGW32__)	
	CfileInfo(WIN32_FIND_DATAW* _data);
#else
	CfileInfo(dirent *_entry, const char* _current_dir);
#endif	
	~CfileInfo();

	inline uint64_t TIME_CREATE() { return m_pkInfo.time_create.GetNum(); }
	inline uint64_t TIME_ACCESS() { return m_pkInfo.time_access.GetNum(); }
	inline uint64_t TIME_WRITE() { return m_pkInfo.time_write.GetNum(); }
	inline uint64_t FILE_SIZE() { return m_pkInfo.file_size.GetNum(); }
	inline const CChar* FILE_NAME() { return m_pkInfo.file_name.c_str(); }
	inline const CChar* LINK_TARGET_NAME() { return m_pkInfo.symlink_target.c_str(); }


	inline bool IsDir() { return (m_pkInfo.file_attrib.is_dir == 0) ? false : true; }
	inline bool IsFile() { return (m_pkInfo.file_attrib.is_file == 0) ? false : true; }	
	inline bool IsSystem() { return (m_pkInfo.file_attrib.is_system == 0) ? false : true; }
	inline bool IsLink() { return (m_pkInfo.file_attrib.is_link == 0) ? false : true; }
	inline bool IsHidden() { return (m_pkInfo.file_attrib.is_hidden == 0) ? false : true; }
	inline bool IsReadOnly() { return (m_pkInfo.file_attrib.is_u_write == 0) ? true : false; }

	inline bool UserCanRead() { return (m_pkInfo.file_attrib.is_u_read == 0) ? false : true; }
	inline bool UserCanWrite() { return (m_pkInfo.file_attrib.is_u_write == 0) ? false : true; }
	inline bool UserCanExec() { return (m_pkInfo.file_attrib.is_u_exec == 0) ? false : true; }

	inline bool GroupCanRead() { return (m_pkInfo.file_attrib.is_g_read == 0) ? false : true; }
	inline bool GroupCanWrite() { return (m_pkInfo.file_attrib.is_g_write == 0) ? false : true; }
	inline bool GroupCanExec() { return (m_pkInfo.file_attrib.is_g_exec == 0) ? false : true; }

	inline bool OtherCanRead() { return (m_pkInfo.file_attrib.is_o_read == 0) ? false : true; }
	inline bool OtherCanWrite() { return (m_pkInfo.file_attrib.is_o_write == 0) ? false : true; }
	inline bool OtherCanExec() { return (m_pkInfo.file_attrib.is_o_exec == 0) ? false : true; }

	inline unsigned short GetAllFilePermission() { return (m_pkInfo.file_attrib.attrib_info >>5); }
};