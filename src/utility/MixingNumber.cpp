#include "pch.h"
#include "customdefine.h"
#include "MixingNumber.h"

SMixingNumber::SMixingNumber() 
{
	m_uNumberinfo.number = 0;
}

SMixingNumber::SMixingNumber(SMixingNumber& _otherObj) 
{
	m_uNumberinfo.number = _otherObj.GetNum();
}

SMixingNumber::SMixingNumber(DWORD _low, DWORD _hi)
{
	m_uNumberinfo.CombinNumber[0] = _low;
	m_uNumberinfo.CombinNumber[1] = _hi;
}

#if defined(__MINGW32__)
SMixingNumber::SMixingNumber(_FILETIME _time)
{
	m_uNumberinfo.CombinNumber[0] = _time.dwLowDateTime;
	m_uNumberinfo.CombinNumber[1] = _time.dwHighDateTime;
}
#endif

SMixingNumber::SMixingNumber(uint64_t _number)
{
	m_uNumberinfo.number = _number;
}

SMixingNumber& SMixingNumber::operator=(const SMixingNumber& _otherObj)
{
	m_uNumberinfo.number = _otherObj.GetNum();
	return *this;
}