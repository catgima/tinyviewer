#include "pch.h"
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <assert.h>

#ifdef __GNUC__
	#include <dirent.h>
	#include <errno.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <string.h>
#else
	#include <fnmatch.h>	
#endif

#include "customdefine.h"

#if __MINGW32__
std::wstring CHARtoWCHAR (const std::string& str, UINT codePage) 
{
	if(str.empty()) 
		return std::wstring();
    int count = MultiByteToWideChar(codePage, 0, str.c_str(), str.size(), NULL, 0);
	std::wstring wstr(count, 0);
    MultiByteToWideChar(codePage, 0, str.c_str(), str.size(), &wstr[0], count);
    return wstr;
}

std::string WCHARtoCHAR (const std::wstring& wstr, UINT codePage)
{
	if(wstr.empty()) 
		return std::string();
    int count = WideCharToMultiByte(codePage, 0, &wstr[0], wstr.size(), NULL, 0, NULL, NULL);
    std::string str(count, 0);
    WideCharToMultiByte(codePage, 0, &wstr[0], wstr.size(), &str[0], count, NULL, NULL);
    return str;
}
#endif


