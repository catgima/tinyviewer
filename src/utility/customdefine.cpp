#include "pch.h"
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <assert.h>

#ifdef __GNUC__
	#include <dirent.h>
	#include <errno.h>
	#include <unistd.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <string.h>
#else
	#include <fnmatch.h>	
#endif

#include "customdefine.h"

void GetWorkPath(CChar* _buf, int _length)
{
#if defined(__MINGW32__)
	_wgetcwd (_buf, _length);
#else
	getcwd (_buf, _length);
#endif
}

void ToLower(CChar *src)
{
	CChar *p = src;
	while (*p) {
		if (*p <= _T('Z') && *p >= _T('A'))
			*p += 32;
		p++;
	}
}

void RStripString(CChar *fname, CChar _symbo)
{
    CChar *end = fname + CStrlen(fname);
    while (end > fname && *end != _symbo) {
    	--end;
    }

    if (end > fname) {
        *end = '\0';
    }
}

int Csprintf (CChar* sDest, size_t len, const CChar* sFormat, ...) 
{
	va_list argList;
	va_start(argList, sFormat);
	size_t nCount = Cvsprintf(sDest, len, sFormat, argList);
	va_end(argList);
	assert (nCount<len && _T("Csprintf buffer overflow!"));
	return (int)nCount;
}

std::string StrW2S (std::wstring src)
{
	if (src.size() == 0)
		return "";

	char* g_pszOriLocale = setlocale (LC_ALL, "");

#if defined(_WIN32) && !defined(__MINGW32__)
	#pragma warning (push)
	#pragma warning (disable:4996)
#endif

	setlocale (LC_ALL, g_pszOriLocale);

    size_t buffersize = (src.size()+1)*2;
    char* buffer = (char*)malloc(buffersize);
	size_t destlen = wcstombs (buffer, src.c_str(), buffersize-1);
    if (destlen == 0 || destlen == (size_t)(-1)) {
		free (buffer);
        return "";
    }
	buffer[destlen] = '\0';
    std::string _buffer = buffer;
    free (buffer);
	return _buffer;

#if defined(_WIN32) && !defined(__MINGW32__)
	#pragma warning (pop)
#endif
}

std::wstring StrS2W (std::string src)
{
	if (src.size() == 0)
		return L"";

	char* g_pszOriLocale = setlocale (LC_ALL, "");

#if defined(_WIN32) && !defined(__MINGW32__)
	#pragma warning (push)
	#pragma warning (disable:4996)
#endif

	setlocale (LC_ALL, g_pszOriLocale);

    size_t buffersize = (src.size()+1)*2;
    wchar_t* buffer = (wchar_t*)malloc (buffersize);
	size_t destlen = mbstowcs (buffer, src.c_str(), buffersize-1);
    if (destlen == 0 || destlen == (size_t)(-1)) {
        free (buffer);
        return L"";
    }

    buffer[destlen] = '\0';
    std::wstring _buffer = buffer;
    free (buffer);
    return _buffer;

#if defined(_WIN32) && !defined(__MINGW32__)
	#pragma warning (pop)
#endif
}

#if defined(__MINGW32__)
//soruce code from Google-glog
bool SafeFNMatch(const CChar* pattern,size_t patt_len,const CChar* str,size_t str_len)
{
	size_t p = 0;
	size_t s = 0;

	while (1)
	{
		if (p == patt_len && s == str_len)
			return true;
			
		if (p == patt_len)
			return false;

		if (s == str_len)
			return (p+1 == patt_len && pattern[p] == _T('*'));

		if (pattern[p] == str[s] || pattern[p] == _T('?')) {
			p += 1;
			s += 1;
			continue;
		}

		if (pattern[p] == _T('*'))
		{
			if (p+1 == patt_len) 
				return true;

			do {
				if (SafeFNMatch(pattern+(p+1), patt_len-(p+1), str+s, str_len-s)) {
					return true;
				}
				s += 1;
			} 
			while (s != str_len);
			return false;
		}
		return false;
	}
}

int fnmatch_win(const CChar *pattern, const CChar *name, int flags)
{
  if(SafeFNMatch(pattern,CStrlen(pattern),name,CStrlen(name)))
    return 0;
  
  return FNM_NOMATCH;
}

#endif

