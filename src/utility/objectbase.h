#pragma once

class ObjectBase
{
	double defaultTimeStart;
	double defaultTimeEnd;

	double timeStart;
	double timeEnd;

public:
	ObjectBase();
	~ObjectBase();

	void ClockStart();
	void ClockEnd();
	double GetTimeSpendFromCPU();
	double GetTimeSpendFromProg();
};