#pragma once

class ObjectBase
{
	intptr_t pMem_address;

	double defaultTimeStart;
	double defaultTimeEnd;

	double timeStart;
	double timeEnd;

	static std::unordered_map<intptr_t, ObjectBase*> BaseObjectList;

public:
	ObjectBase();
	~ObjectBase();

	void ClockStart();
	void ClockEnd();
	double GetTimeSpendFromCPU();
	double GetTimeSpendFromProg();
};