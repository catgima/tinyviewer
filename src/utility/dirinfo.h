#pragma once

class CfileInfo;

struct SDir 
{
	unsigned int nID;
	SDir* pPrevDir;
	CfileInfo* pDirInfo;
	std::list<SDir*> SubDirList;
	std::list<CfileInfo*> FileList;

	SDir();
	~SDir(); 
};

//store dir name db
typedef std::unordered_map<CStdString, unsigned int> DirNameMap;
typedef std::unordered_map<CStdString, SDir*> AllDirTable;

class CDirinfo : public ObjectBase
{
	SDir* m_nCurrDir;
	std::list<SDir*> m_RootDirs;
	static std::vector<CStdString> DirNameTable;
	static DirNameMap NameMapTable; 
	static AllDirTable AllDirInfoTable;

public:
	CDirinfo();
	~CDirinfo();

	SDir* EnterDir(CStdString _PathString);
	SDir* LeaveDir();

	CStdString GetPath(SDir* _dir = 0);
	SDir* GetDir(CStdString& _path);

	void AddDirInfo(SDir* _dir, CfileInfo *_info);
	void AddFileInfo(SDir* _dir, CfileInfo *_info);
	void AddSubDirInfo(SDir* _parentDir, CfileInfo *_info);

	inline SDir *GetDirInfo() { return m_nCurrDir; }
	inline std::list<SDir*>& GetRootInfo() { return m_RootDirs; }
};