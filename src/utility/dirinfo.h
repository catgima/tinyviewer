#pragma once

class CfileInfo;

struct SDir 
{
	unsigned int nID;
	SDir* pPrevDir;
	CfileInfo* pDirInfo;
	std::vector<SDir*> SubDirList;
	std::vector<CfileInfo*> FileList;

	SDir();
	~SDir(); 
};

typedef std::unordered_map<CStdString, unsigned int> DirNameMap;

class CDirinfo : public ObjectBase
{
	SDir* m_nCurrDir;
	std::vector<SDir*> m_RootDirs;

public:
	CDirinfo();
	~CDirinfo();

	static std::vector<CStdString> DirNameTable;
	static DirNameMap NameMapTable;

	SDir* EnterDir(CStdString _PathString);
	SDir* LeaveDir();

	inline SDir *GetDirInfo() { return m_nCurrDir; }
	inline std::vector<SDir*>& GetRootInfo() { return m_RootDirs; }

	CStdString GetCurrentFullPath();
	CStdString GetPath(SDir* _dir = 0);
	void AddDirInfo(SDir* _dir, CfileInfo *_info);
	void AddFileInfo(SDir* _dir, CfileInfo *_info);
};