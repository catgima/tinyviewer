#include "pch.h"

#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <wchar.h>
#include <immintrin.h>
#include <errno.h>

#include "objectbase.h"
#include "MixingNumber.h"
#include "dirinfo.h"
#include "fileinfo.h"
#include "filesearch.h"

CFileSearch::CFileSearch()
{
	DirManager = new CDirinfo();
	int cur_path_len = 0;

#if defined(__MINGW32__)	
	cur_path_len = PATH_MAX;
#else
	if ((cur_path_len = pathconf(".", _PC_PATH_MAX)) == -1) {
		perror("Couldn`t get current working path length");
		return;
	}	
#endif	

	std::cout << "Current path length is " << cur_path_len << std::endl;
}

CFileSearch::~CFileSearch()
{
	delete DirManager;
}

void CFileSearch::GetAllFilesFromPath(std::vector<CStdString>& _searchPath)
{
	ClockStart();

	CChar nWorkpath[PATH_MAX];
	GetWorkPath (&nWorkpath[0], PATH_MAX);

	std::vector<CStdString>::iterator nPathIt = _searchPath.begin();
	for (; nPathIt != _searchPath.end(); ++nPathIt) {
		wprintf(L"_____________________________________\n\n\n");
		SearchDir(nPathIt->c_str(), false);
	}

	ClockEnd();
	CChdir(nWorkpath);
} 

void CFileSearch::GetAllFilesFromPath(CStdString& _searchPath)
{
	ClockStart();

	CChar nWorkpath[PATH_MAX];
	GetWorkPath (&nWorkpath[0], PATH_MAX);

	CChar CurrentDir[PATH_MAX] = {0};
  	GetCurrentDir(CurrentDir, PATH_MAX);
	
	CStdString BeginSearchDir = _searchPath;
#if !defined(__MINGW32__)	
	if ((_searchPath[0] == _T('.') && _searchPath.size() == 1) ||
		(_searchPath[0] == _T('.') && (_searchPath[1] == _T('/') || _searchPath[1] == _T('\\')))
	) {
		BeginSearchDir = CurrentDir;
	}
#endif	
	
	SearchDir(BeginSearchDir.c_str(), true);

	ClockEnd();
	CChdir(CurrentDir);
}

#if defined(__MINGW32__)
SDir* CFileSearch::SearchDir(const CChar *_DirPath, bool IsRecursive)
{
	if (CChdir(_DirPath) != 0) {
		wprintf(L"Error errno: %d file: %ls\n", errno, _DirPath);
		return 0;
	}

	SDir* nDir = DirManager->EnterDir(_DirPath);
	if (!nDir->pPrevDir) {
		WIN32_FIND_DATAW nRootFindData = {0};
		CStrcpy (nRootFindData.cFileName, _DirPath);
		DirManager->AddDirInfo(nDir, new CfileInfo(&nRootFindData));
	}

	WIN32_FIND_DATAW nFindData;
	HANDLE hFile = FindFirstFileW(L"*.*", &nFindData);
   	if (hFile == INVALID_HANDLE_VALUE) {
		wprintf(L"opendir() error : %ls\n", _DirPath);
		DirManager->LeaveDir();
		chdir("..");
		return nDir;
	}

	do	{
		if(nFindData.cFileName[0] == L'.' && nFindData.cFileName[1] == L'.' && nFindData.cFileName[2] == L'\0')
			continue;

		if (nFindData.cFileName[0] == L'.' && nFindData.cFileName[1] == L'\0') {
			if (!nDir->pPrevDir && !nDir->pDirInfo) {
				DirManager->AddDirInfo(nDir, new CfileInfo(&nFindData));
			}
			continue;
		}

		CfileInfo* nInfo = new CfileInfo(&nFindData);

		if ((nFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
		{
			FindDirRules(nDir, nInfo);
			
			if (IsRecursive) {
				SDir* nLastDir = SearchDir(nFindData.cFileName, IsRecursive);
				if (nLastDir) {
					DirManager->AddDirInfo(nLastDir, nInfo);
				}
				else {
					delete nInfo; //chdir fail , nInfo remove
				}
				continue;
			}
			else {
				DirManager->AddDirInfo(nDir, nInfo);
			}
		}
		else {
			DirManager->AddFileInfo(nDir, nInfo);
			FindFileRules(nDir, nInfo);
		}

	} while (FindNextFileW(hFile, &nFindData) != 0);

	FindClose(hFile);
	DirManager->LeaveDir();
	chdir("..");
	return nDir;
}
#else
SDir* CFileSearch::SearchDir(const CChar *_DirPath, bool IsRecursive)
{
	if (CChdir(_DirPath) != 0) {
		printf("chdir error errno: %d file: %s\n", errno, _DirPath);
		return 0;
	}
	
	CStdString nFullPath = DirManager->GetCurrentFullPath();
	SDir* nDir = DirManager->EnterDir(_DirPath);
		
	DIR *dir = opendir("./");
	if (!dir) {
		if (errno == EPERM) {
			printf("opendir error errno: %d , Operation not permitted: %s\n", errno, _DirPath);
		}
		else if (errno == EACCES) {
			printf("opendir error errno: %d , Permission denied: %s\n", errno, _DirPath);
		}
		else {
			printf("opendir error errno: %d %s\n", errno, _DirPath);
		}
		
		DirManager->LeaveDir();
		chdir("..");
		return nDir;
	}

	struct dirent *entry;
	while ((entry = readdir(dir)) != NULL)
	{
		//ignore previous directory
		if(entry->d_name[0] == '.' && entry->d_name[1] == '.' && entry->d_name[2] == '\0')
			continue;

		// save root directory info else ignore
		if (entry->d_name[0] == '.' && entry->d_name[1] == '\0') {
			if (!nDir->pPrevDir) {
				DirManager->AddDirInfo(nDir, new CfileInfo(entry, nFullPath.c_str()));
			}
			continue;
		}

		CfileInfo* nInfo = new CfileInfo(entry, nFullPath.c_str());
		if ((entry->d_type & DT_DIR) == DT_DIR)
		{
			FindDirRules(nDir, nInfo);
			
			if (IsRecursive) {
				SDir* nLastDir = SearchDir(entry->d_name, IsRecursive);
				if (nLastDir) {
					DirManager->AddDirInfo(nLastDir, nInfo);
				}
				else {
					delete nInfo; //chdir fail , nInfo remove
				}
				continue;
			}
			else {
				DirManager->AddDirInfo(nDir, nInfo);
			}
		}
		else {
			DirManager->AddFileInfo(nDir, nInfo);
			FindFileRules(nDir, nInfo);
		}
	}
	
	closedir(dir);
	DirManager->LeaveDir();
	chdir("..");
	return nDir;
}
#endif

void CFileSearch::FindFileRules(SDir* _dir, CfileInfo* _info)
{
	//ToLower((CChar*)_info->FILE_NAME().c_str());
	//ShowAttribString(_info);
}

void CFileSearch::FindDirRules(SDir* _dir, CfileInfo* _info)
{
	
	//ShowAttribString(_info);
}

void CFileSearch::ShowAttribString(CfileInfo* _info, SDir* _dir)
{
	CChar AttribString[10] = {0};
	CChar StatusString[4];

	int idx = 0;
 	if (_info->IsDir())	
	 	StatusString[idx++] = _T('d');
	else 
		StatusString[idx++] = _T('-');

	if (_info->IsHidden()) 
		StatusString[idx++] = _T('h');

	if (_info->IsLink()) 
		StatusString[idx++] = _T('l');

	StatusString[idx] = _T('\0');
 
	unsigned short nAllAttrib = _info->GetAllFilePermission();
	for (int i=0; i<9; i++) 
	{
		if ((nAllAttrib & (1 << i)) == 0) {
			AttribString[i] = _T('-');
			continue;
		}
		if (i%3 == 0) AttribString[i] = _T('r');
		if (i%3 == 1) AttribString[i] = _T('w');
		if (i%3 == 2) AttribString[i] = _T('x');
	}

	if (_info->IsHidden())
		return;

#if !defined(__MINGW32__)	
	#if defined (__APPLE_CC__) || defined (__APPLE__)
		if (_info->IsLink())
			printf("\x1b[32m %-3s %s\t %10llu \t %s/%s ---> %s\x1b[37m\n", StatusString,
				AttribString, _info->FILE_SIZE(), DirManager->GetPath(_dir).c_str(), 
				_info->FILE_NAME(), _info->LINK_TARGET_NAME());
		else
			printf(" %-3s %s\t %10llu \t %s/%s\n", StatusString, AttribString,
				_info->FILE_SIZE(), DirManager->GetPath(_dir).c_str(), _info->FILE_NAME());
	#else
		if (_info->IsLink())
			printf("\x1b[32m %-3s %s\t %10lu \t %s/%s ---> %s\x1b[37m\n", StatusString,
				AttribString, _info->FILE_SIZE(), DirManager->GetPath(_dir).c_str(), 
				_info->FILE_NAME(), _info->LINK_TARGET_NAME());
		else
			printf(" %-3s %s\t %10lu \t %s/%s\n", StatusString, AttribString,
				_info->FILE_SIZE(), DirManager->GetPath(_dir).c_str(), _info->FILE_NAME());
	#endif
#else
	wprintf(L" %-3ls %ls\t %10llu \t %ls\\%ls\n", StatusString, AttribString,
			_info->FILE_SIZE(), DirManager->GetPath(_dir).c_str(), _info->FILE_NAME());
#endif	

}

void CFileSearch::PrintErrno(char* TagString)
{
}

std::vector<SDir*>& CFileSearch::GetRootDir() 
{ 
	return DirManager->GetRootInfo();
}

