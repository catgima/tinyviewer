#include "pch.h"

#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <wchar.h>
//#include <immintrin.h>
#include <errno.h>

#include "systeminfo.h"
#include "MixingNumber.h"
#include "dirinfo.h"
#include "fileinfo.h"
#include "pathutil.h"
#include "stringutil.h"
#include "filesearch.h"

CFileSearch::CFileSearch()
{
	m_pDirManager = new CDirinfo();
	m_pPathUtil = &PathUtil::instance();
}

CFileSearch::~CFileSearch()
{
	delete m_pDirManager;
}

#if defined(__MINGW32__)
SDir* CFileSearch::SearchDir(const CChar *_DirPath, SBitFieldSearchRuleFlag* _flag)
{
	if (CChdir(_DirPath) != 0) {
		//wprintf(L"Error errno: %d file: %ls\n", errno, _DirPath);
		return 0;
	}

	SDir* nDir = m_pDirManager->EnterDir(_DirPath);
	if (!nDir->pPrevDir) {
		WIN32_FIND_DATAW nRootFindData = {0};
		CStrcpy (nRootFindData.cFileName, _DirPath);
		m_pDirManager->AddDirInfo(nDir, new CfileInfo(&nRootFindData));
	}

	WIN32_FIND_DATAW nFindData = {0};
	HANDLE hFile = FindFirstFileW(L"*.*", &nFindData);
   	if (hFile == INVALID_HANDLE_VALUE) {
		wprintf(L"opendir() error : %ls\n", _DirPath);

		m_pDirManager->LeaveDir();
		if (nDir->pPrevDir) {
			chdir("..");
		}
		return nDir;
	}

	do	{
		if (nFindData.cFileName[0] == L'.' && nFindData.cFileName[1] == L'.' && nFindData.cFileName[2] == L'\0')
			continue;
		
		if (nFindData.cFileName[0] == L'.' && nFindData.cFileName[1] == L'\0') {
			if (!nDir->pPrevDir && !nDir->pDirInfo) {
				m_pDirManager->AddDirInfo(nDir, new CfileInfo(&nFindData));
			}
			continue;
		}

		CfileInfo* nInfo = new CfileInfo(&nFindData);

		if ((nFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY) {
			MatchFindDirRules(nDir, nInfo, _flag);
		}
		else {
			MatchFindFileRules(nDir, nInfo, _flag);
		}

	} while (FindNextFileW(hFile, &nFindData) != 0);

	FindClose(hFile);

	m_pDirManager->LeaveDir();
	if (nDir->pPrevDir) {
		chdir("..");
	}
	return nDir;
}
#else
SDir* CFileSearch::SearchDir(const CChar *_DirPath, SBitFieldSearchRuleFlag* _flag)
{
	if (CChdir(_DirPath) != 0) {
		//printf("chdir error errno: %d file: %s\n", errno, _DirPath);
		return 0;
	}	
	
	SDir* nDir = m_pDirManager->EnterDir(_DirPath);
	CStdString nFullPath = m_pPathUtil->GetCurrentFullPath();
			
	DIR *dir = opendir("./");
	if (!dir) {
		if (errno == EPERM) {
			printf("opendir error errno: %d , Operation not permitted: %s\n", errno, _DirPath);
		}
		else if (errno == EACCES) {
			printf("opendir error errno: %d , Permission denied: %s\n", errno, _DirPath);
		}
		else {
			printf("opendir error errno: %d %s\n", errno, _DirPath);
		}

		m_pDirManager->LeaveDir();		
		if (nDir->pPrevDir) {
			chdir("..");
		}
		
		return nDir;
	}

	struct dirent *entry;
	while ((entry = readdir(dir)) != NULL)
	{
		//ignore previous directory
		if(entry->d_name[0] == '.' && entry->d_name[1] == '.' && entry->d_name[2] == '\0')
			continue;

		// save root directory info else ignore
		if (entry->d_name[0] == '.' && entry->d_name[1] == '\0') {
			if (!nDir->pPrevDir) {
				m_pDirManager->AddDirInfo(nDir, new CfileInfo(entry, nFullPath.c_str()));
			}
			continue;
		}

		CfileInfo* nInfo = new CfileInfo (entry, nFullPath.c_str());
		if ((entry->d_type & DT_DIR) == DT_DIR) {
			MatchFindDirRules (nDir, nInfo, _flag);
		}
		else {
			MatchFindFileRules (nDir, nInfo, _flag);
		}
	}
	
	closedir(dir);

	m_pDirManager->LeaveDir();
	if (nDir->pPrevDir) {
		chdir("..");
	}
	return nDir;
}
#endif

void CFileSearch::MatchFindFileRules(SDir* _dir, CfileInfo* _info, SBitFieldSearchRuleFlag* _flag)
{
	m_pDirManager->AddFileInfo(_dir, _info);
}

void CFileSearch::MatchFindDirRules(SDir* _dir, CfileInfo* _info, SBitFieldSearchRuleFlag* _flag)
{
	if (_flag->recursive) {
		SDir* nLastDir = SearchDir(_info->FILE_NAME().c_str(), _flag);
		if (nLastDir) {
			m_pDirManager->AddDirInfo (nLastDir, _info);
			return;
		}
		return;
	}

	m_pDirManager->AddSubDirInfo(_dir, _info);
}

std::list<SDir*>& CFileSearch::GetRootDir() 
{ 
	return m_pDirManager->GetRootInfo();
}

