#pragma once

#define  FNM_PATHNAME  (1 << 0) /* No wildcard can ever match `/'. */
#define  FNM_NOESCAPE  (1 << 1) /* Backslashes don't quote special chars. */
#define  FNM_PERIOD    (1 << 2) /* Leading `.' is matched only explicitly. */
#define  FNM_NOMATCH    1

class StringUtil : public Singleton<StringUtil>, ObjectBase
{
	bool SafeFNMatch(const CChar* pattern,size_t patt_len,const CChar* str,size_t str_len);

public:
	StringUtil() {}
	~StringUtil() {}
	
	int StringMatch(const CChar *pattern, const CChar *name, int flags = 0);
	void ReverseStripString(CChar *fname, CChar _symbo);
	void SplitNameTag(const CStdString& _filename, std::vector<CStdString>& nList);
	bool CheckRegexSyntaxValid(CStdString& matchstring);
	std::wstring s2ws(const std::string& str);
	std::string ws2s(const std::wstring& wstr);
};

template <typename T> static std::vector <T>& StrSplit (T inStr, T sepStr, std::vector<T>& outStrVec)  
{
	size_t token = 0;
	typename T::size_type found = inStr.find_first_of (sepStr);
	while (found != T::npos) {
		outStrVec.push_back (inStr.substr (token, found - token));
		token = found + 1;
		found = inStr.find_first_of (sepStr, token);
	}
	T str = inStr.substr (token, inStr.size());
	if (str.size() > 0) {
		outStrVec.push_back (inStr.substr (token, inStr.size()));
	}
	return outStrVec;
}

template <typename T> static void ReplaceKeyChar(T oldKey, T newKey, T* SrcStr) 
{
	size_t i = 0;
	while (SrcStr[i]){
		if (SrcStr[i] == oldKey)
			SrcStr[i] = newKey;
		i++;
	}
}

template <typename T> static T RemoveStringSpace (T &s) 
{
	if (!s.empty()) {
	    s.erase (0, s.find_first_not_of(_T(" ")));
		s.erase (s.find_last_not_of(_T(" ")) + 1);
	}
	return s;
}

template <typename T> static void EraseSpecificChar (T* instr, T* sepStr)
{
	char* strptr = instr;
	size_t offset = 0;
	while (*strptr) {
		const char* ptr = sepStr;
		while (*ptr) {
			if (*strptr == *ptr) {
				strptr++;
				ptr = sepStr;
			}
			else {
				ptr++;
			}
		}
		instr[offset] = *strptr;
		if (*strptr) {
			strptr++;
			offset++;
		}
	}
}