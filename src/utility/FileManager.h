#pragma once

class CFileSearch;
class FileManager : public ObjectBase
{
	CFileSearch* m_File;
	unsigned int count;

public:
	FileManager();
	~FileManager();

	void GetAllFileInfo(CStdString _searchPath);
	void CreateFileList();
	unsigned int GetNameTableCount();
	unsigned int GetDirTableCount();

private:
	void GetDirInfoRecursive(SDir* _nDir);	
};