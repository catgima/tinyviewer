#pragma once

class CfileInfo;
class CDirinfo;
class CFileSearch;
class FileUtil;
class StringUtil;

struct yyjson_mut_val;
struct yyjson_mut_doc;
struct yyjson_doc;
struct yyjson_val;

// Folder count information
struct SFolderInfo
{
	unsigned int filecount;
	unsigned int dircount;
	uint64_t totalsize;

	void ClearAll() { 
		filecount = 0;
		dircount = 0;
		totalsize = 0;
	}
	SFolderInfo() : filecount(0), dircount(0), totalsize(0) {}
};

struct SCommandInfo 
{
	CStdString db_name;
	std::vector<CStdString> macth_string;
	SBitFieldSearchRuleFlag flag;
	SDir* rootdir;
	std::vector<SPathFileInfo> findlist;
	SCommandInfo() {
		flag.attrib_info = 0;
		rootdir = 0;
	}
};

class FileManager : public ObjectBase
{
	struct DupTagNameCheck
	{
		std::map<CStdString, CfileInfo*> TagNameTable;
		std::vector<CfileInfo*> DupTagNameList;
		
		void Clear () {
			TagNameTable.clear();
			DupTagNameList.clear();
		}
	};

	CFileSearch* m_File;
	std::vector<CfileInfo*> SortFilelist;

	yyjson_mut_doc* m_pkMutDoc;
	yyjson_mut_val* m_pkMutRoot;
	
	std::unordered_map <std::string, yyjson_mut_val*> jsonDataMap;

	void GetDirInfoRecursive(SCommandInfo* _filterinfo, SDir* _nDir, SFolderInfo* _info = 0);	
	bool GetFolderInfo(SCommandInfo* _filterinfo, CStdString _path, SFolderInfo& _info);
	
protected:	
	FileUtil* m_pkFileUtil;
	CDirinfo* m_Dir;
	DupTagNameCheck mkDupCheck;
	StringUtil* pkStringUtil;

	virtual void OnFileInfoOutput (SCommandInfo* _filterinfo, CfileInfo* _info);
	virtual void OnDirFileInfoOutputStart (SCommandInfo *_filterinfo, SFolderInfo* _info);
	virtual void OnDirFileInfoOutputEnd (SCommandInfo *_filterinfo, SFolderInfo* _info);
	virtual void OnFileInfoPrint (SCommandInfo *_filterinfo, CfileInfo* _info) {}

public:
	FileManager();
	~FileManager();

	void GetAllFileInfo (SCommandInfo* _info);
	void CreateFileList (SCommandInfo* _info);
	void CheckDupTagName (CfileInfo* _fileinfo);
	bool CheckMatchString (SCommandInfo* _filterinfo, const CStdString& filename);
};

