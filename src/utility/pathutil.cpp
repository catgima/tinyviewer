#include "pch.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include "systeminfo.h"

#if !defined (__MINGW32__)
	#include <unistd.h>  
	#include <pwd.h>  
#endif  

#include "pathutil.h"

CStdString PathUtil::GetWorkPath()
{
	CChar nWorkpath[SystemInfo::instance().StringMaxLength];
#if defined(__MINGW32__)
	_wgetcwd (nWorkpath, SystemInfo::instance().StringMaxLength);
#else
	getcwd (nWorkpath, SystemInfo::instance().StringMaxLength);
#endif
	return nWorkpath;
} 

bool PathUtil::IsPathIsDir(const CChar* _PathName)
{
#if defined (__MINGW32__)
	DWORD dwAttrib = GetFileAttributesW (_PathName);
	return (dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
#else

	int nStatResult = 0;
	#if defined (__APPLE_CC__) || defined (__APPLE__)
		struct stat nStatinfo;
		nStatResult = lstat (_PathName, &nStatinfo);
	#else
		struct stat64 nStatinfo;
		nStatResult = stat64 (_PathName, &nStatinfo);
	#endif
		if (nStatResult == 0) {
			return ((nStatinfo.st_mode & S_IFMT) == S_IFDIR) ? true : false;
		}
#endif		
  	return false;
}

void PathUtil::SplitPath(const CChar* pcFullPath, SPathFileInfo& _info)
{
	_info.IsDir = IsPathIsDir(pcFullPath);
	CChar* nFullPath = (CChar*)pcFullPath;

	int i = 0;
	int DriveEnd = 0;
	int FileStart = 0;
	int FileEnd = 0;
	int n = 0;
	
	while (nFullPath[i])	
	{
		switch(nFullPath[i])	{
			case _T(':'):
				DriveEnd=i+1;
				break;
			case _T('\\'):
				nFullPath[i] = _T('/');
			case _T('/'):
				FileStart=i+1;
				break;
			case _T('.'):
				FileEnd=i;
				break;
		}
		i++;
	}

	if(DriveEnd>0) {
		CStrncpy(_info.drv, pcFullPath, DriveEnd);
	}
	_info.drv[DriveEnd] = _T('\0');
		
	if(FileEnd>0) {
		n = i-FileEnd;
		CStrncpy(_info.ext, pcFullPath+FileEnd, n);
	}
	_info.ext[n] = _T('\0');
	
	if(FileStart>0)	{
		if (_info.IsDir) 
			FileStart = i;
		n = FileStart-DriveEnd;
		CStrncpy(_info.dir, pcFullPath+DriveEnd, n);
	}
	_info.dir[n] = _T('\0');

	if (_info.IsDir) {
		_info.file[0] = _T('\0');
		return;
	}

	n=((FileEnd>0)?FileEnd:i)-FileStart;
	if(n>0)	{
		CStrncpy(_info.file, pcFullPath+FileStart, n);
	}
	_info.file[n] = _T('\0');
}

CStdString PathUtil::getUserDir()  
{  
#if defined (__MINGW32__)
	CChar *name = _wgetenv(_T("USERNAME"));
	return name;
#else 
    uid_t userid;  
    struct passwd* pwd;  
    userid=getuid();  
    pwd=getpwuid(userid);  
    return pwd->pw_name;  
#endif  
}  

CStdString PathUtil::GetCurrentFullPath()
{
	CChar nCurrentDir[PATH_MAX] = {0};
	GetCurrentDir(nCurrentDir, PATH_MAX);
	return nCurrentDir;
}





