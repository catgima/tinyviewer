#include "pch.h"
#include <dirent.h>
#include "customdefine.h"
#include "MixingNumber.h"
#include "fileinfo.h"
#include "dirinfo.h"

std::vector<CStdString> CDirinfo::DirNameTable;
DirNameMap CDirinfo::NameMapTable;
AllDirTable CDirinfo::AllDirInfoTable;

SDir::SDir() 
	: nID(0)
	, pDirInfo(0)
	, pPrevDir(0) 
{}

SDir::~SDir() 
{
	DeleteVectorContent(FileList);
	DeleteVectorContent(SubDirList);
	if (pDirInfo) {
		delete pDirInfo;
		pDirInfo = 0;
	}
}

CDirinfo::CDirinfo()
	: m_nCurrDir(0)
{	
}

CDirinfo::~CDirinfo() 
{
	DeleteVectorContent(m_RootDirs);
}

SDir* CDirinfo::EnterDir(CStdString _PathString) 
{
	if(_PathString.back() == _T('/') || _PathString.back() == _T('\\'))
		_PathString.pop_back();	
	
	unsigned int nNameID = 0;	
	DirNameMap::iterator name_it = NameMapTable.find(_PathString);
	if (name_it == NameMapTable.end()) {
		DirNameTable.push_back (_PathString);
		nNameID = (unsigned int)DirNameTable.size()-1;
		NameMapTable[_PathString] = nNameID;
	}
	else {
		nNameID = name_it->second;
	}

	SDir *nNewDir = new SDir();
	nNewDir->nID = nNameID;

	if (_PathString != _T("."))
		nNewDir->pPrevDir = m_nCurrDir; 

	m_nCurrDir = nNewDir;
	AllDirInfoTable.insert(std::make_pair(GetPath(nNewDir), nNewDir));
	return m_nCurrDir;
}

SDir* CDirinfo::LeaveDir()
{
	if (m_nCurrDir->pPrevDir) {
		m_nCurrDir = m_nCurrDir->pPrevDir;
	}
	else {
		m_nCurrDir = 0;
	}
	return m_nCurrDir;
}

void CDirinfo::AddSubDirInfo(SDir* _parentDir, CfileInfo *_info)
{
	CStdString nPathString = _info->FILE_NAME();

	unsigned int nNameID = 0;	
	DirNameMap::iterator name_it = NameMapTable.find(nPathString);
	if (name_it == NameMapTable.end()) {
		DirNameTable.push_back (nPathString);
		nNameID = (unsigned int)DirNameTable.size()-1;
		NameMapTable[nPathString] = nNameID;
	}
	else {
		nNameID = name_it->second;
	}

	SDir *nNewDir = new SDir();
	_info->SetParentDir (nNewDir);
	nNewDir->nID = nNameID;
	nNewDir->pPrevDir = _parentDir;
	nNewDir->pDirInfo = _info;
	_parentDir->SubDirList.push_back(nNewDir);
}

SDir* CDirinfo::GetDir(CStdString& _path)
{
	AllDirTable::iterator it = AllDirInfoTable.find(_path);
	if (it == AllDirInfoTable.end())
		return 0;

	return it->second;
}

void CDirinfo::AddDirInfo(SDir* _dir, CfileInfo *_info)
{
	_info->SetParentDir (_dir);
	if (!_dir->pDirInfo) {
		_dir->pDirInfo = _info;
	}

	if (!_dir->pPrevDir) {
		m_RootDirs.push_back(_dir);
		return;
	}

	m_nCurrDir->SubDirList.push_back(_dir);
}

void CDirinfo::AddFileInfo(SDir* _dir, CfileInfo *_info)
{
	_info->SetParentDir (_dir);
	_dir->FileList.push_back(_info);
}

CStdString CDirinfo::GetPath(SDir* _dir) 
{
	CStdString nFullPath;

	if (_dir)
	{
		SDir *ndir = (_dir == 0) ? m_nCurrDir : _dir;
		if (!ndir || DirNameTable.size() == 0)
			return nFullPath;

		while (ndir != 0) 
		{
			CStdString nPathTemp = DirNameTable.at(ndir->nID);
			if(nFullPath.size() == 0 || nPathTemp.back() == _T('/') || nPathTemp.back() == _T('\\')) {
				nFullPath = nPathTemp + nFullPath;
			}
			else {
				nFullPath = nPathTemp + _T("/") + nFullPath;
			}
			ndir = ndir->pPrevDir;
		}	
	}
	
	return nFullPath; 
} 



