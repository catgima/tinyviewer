#include "pch.h"

#if defined(__MINGW32__)
	#include <fcntl.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <io.h>
	#include <windows.h>
    #include <unistd.h>
#endif	

#include <sys/stat.h>
#include <sys/types.h>
#include "systeminfo.h"

SystemInfo::SystemInfo()
{
}

SystemInfo::~SystemInfo()
{
}

void SystemInfo::Init()
{
#if defined(__MINGW32__)
    _setmode(_fileno(stdout), _O_WTEXT);
#endif
    
    setlocale(LC_ALL, "");
#if __MINGW32__
	SetConsoleOutputCP (950);
#endif	
    GetSystemBaseInfo();
}

void SystemInfo::GetSystemBaseInfo()
{
    printf("LC_ALL: %s\n", setlocale(LC_ALL, NULL));
    printf("LC_CTYPE: %s\n", setlocale(LC_CTYPE, NULL));

	//check this program is 32 or 64bit
    void* number =  0;
    int size = sizeof(&number);
    if (size==4) {
        wprintf(L"32 bit:%d\n", size);
    }else {
	    wprintf(L"64 bit:%d\n", size);
    }

    wprintf(L"remain total memory: %llu\n\n\n", getTotalSystemMemory());

#if defined(__MINGW32__)	
	StringMaxLength = PATH_MAX;
#else
	if ((StringMaxLength = pathconf(".", _PC_PATH_MAX)) == -1) {
		perror("Couldn`t get current working path length");
		return;
	}	
#endif

    
	/*
    SRes result;
    //s.GetAllFilesFromPath(nArgList); 

    result.cpu = nFileMgr.GetTimeSpendFromCPU();
    result.prog = nFileMgr.GetTimeSpendFromProg();

    wprintf (L"_________________________________________________\n");
    wprintf (L"程式執行間隔時間：%f\n", result.cpu);
    wprintf (L"進行運算所花費的時間：：%f\n", result.prog);
	*/
}

unsigned long long SystemInfo::getTotalSystemMemory()
{
#if defined(__MINGW32__)    
    MEMORYSTATUSEX status;
    status.dwLength = sizeof(status);
    GlobalMemoryStatusEx(&status);
    return status.ullTotalPhys;
#else
    long pages = sysconf(_SC_PHYS_PAGES);
    long page_size = sysconf(_SC_PAGE_SIZE);
    return pages * page_size;
#endif    
    return 0;
}
