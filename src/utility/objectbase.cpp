#include "pch.h"
#include <time.h>
#include <stdio.h>
#include "objectbase.h"

std::unordered_map<intptr_t, ObjectBase*> ObjectBase::BaseObjectList;

ObjectBase::ObjectBase()
{
	defaultTimeStart = clock();
	pMem_address = static_cast<intptr_t>(reinterpret_cast<intptr_t>(this));
	BaseObjectList.insert (std::make_pair(pMem_address, this));
}

ObjectBase::~ObjectBase()
{
	defaultTimeEnd = clock();

	//printf ("程式執行間隔時間：%f\n", defaultTimeStart/CLOCKS_PER_SEC);
    //printf ("進行運算所花費的時間：：%f\n", (defaultTimeEnd-defaultTimeStart)/CLOCKS_PER_SEC);
	std::unordered_map<intptr_t, ObjectBase*>::iterator memIt = BaseObjectList.find (pMem_address);
	if (memIt != BaseObjectList.end()) {
		BaseObjectList.erase (pMem_address);
	}
}

void ObjectBase::ClockStart()
{
	timeStart = clock();
}

void ObjectBase::ClockEnd()
{
	timeEnd = clock();
} 

double ObjectBase::GetTimeSpendFromCPU() 
{
	return (double)clock()/CLOCKS_PER_SEC;
}

double ObjectBase::GetTimeSpendFromProg()
{
	return (timeEnd-timeStart)/CLOCKS_PER_SEC;
}