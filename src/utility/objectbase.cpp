#include "pch.h"
#include <time.h>
#include <stdio.h>
#include "objectbase.h"


ObjectBase::ObjectBase()
{
	defaultTimeStart = clock();
}

ObjectBase::~ObjectBase()
{
	defaultTimeEnd = clock();

	//printf ("程式執行間隔時間：%f\n", defaultTimeStart/CLOCKS_PER_SEC);
    //printf ("進行運算所花費的時間：：%f\n", (defaultTimeEnd-defaultTimeStart)/CLOCKS_PER_SEC);
}

void ObjectBase::ClockStart()
{
	timeStart = clock();
}

void ObjectBase::ClockEnd()
{
	timeEnd = clock();
} 

double ObjectBase::GetTimeSpendFromCPU() 
{
	return (double)clock()/CLOCKS_PER_SEC;
}

double ObjectBase::GetTimeSpendFromProg()
{
	return (timeEnd-timeStart)/CLOCKS_PER_SEC;
}