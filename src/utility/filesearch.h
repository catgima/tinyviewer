#pragma once

class CDirinfo;
class CfileInfo;
class PathUtil;
struct SDir;
struct SPathFileInfo;

union SBitFieldSearchRuleFlag
{
	unsigned int attrib_info; 
	struct {
		unsigned char read_db : 1;
		unsigned char write_db : 1;
		unsigned char finddup : 1;
		unsigned char sort : 1;
		unsigned char duptag_check : 1;
		unsigned char recursive : 1;
		unsigned char show_full_info : 1;
		unsigned char show_dir_only : 1;
		unsigned char show_file_only : 1;
		unsigned char show_path : 1;
		unsigned char make_file_list : 1;
		unsigned char show_size_unit : 2;	// 0=Byte 1=kilo 2=Mega 3=Giga
		unsigned char use_match_search : 3; // 0=disable 1=ignorecase 2=lowcase 3=regexmatch 4=regexsearch
	};
};

class CFileSearch : public ObjectBase
{
private:	
	CDirinfo* m_pDirManager;
	PathUtil* m_pPathUtil;

	void MatchFindFileRules(SDir* _dir, CfileInfo* _info, SBitFieldSearchRuleFlag* _flag);
	void MatchFindDirRules(SDir* _dir, CfileInfo* _info, SBitFieldSearchRuleFlag* _flag);

public:
	CFileSearch();
	~CFileSearch();

	SDir* SearchDir(const CChar *_DirPath, SBitFieldSearchRuleFlag* _flag);
	std::list<SDir*>& GetRootDir(); 
	CDirinfo* GetDirManager() { return m_pDirManager; }
};


