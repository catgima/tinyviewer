#pragma once

class CDirinfo;
class CfileInfo;
struct SDir;
class CFileSearch : public ObjectBase
{
	CDirinfo* DirManager;

	SDir* SearchDir(const CChar *_DirPath, bool IsRecursive);
	void FindFileRules(SDir* _dir, CfileInfo* _info);
	void FindDirRules(SDir* _dir, CfileInfo* _info);
	void PrintErrno(char* TagString);

public:
	CFileSearch();
	~CFileSearch();

	void GetAllFilesFromPath(std::vector<CStdString>& _searchPath);
	void GetAllFilesFromPath(CStdString& _searchPath);
	void ShowAttribString(CfileInfo* _info, SDir* _dir = 0);
	std::vector<SDir*>& GetRootDir(); 
};


