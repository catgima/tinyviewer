#include "pch.h"
#include <fstream>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "MixingNumber.h"
#include "fileinfo.h"

CfileInfo::CfileInfo() 
	: m_pkParentDir (0)
	, m_pkDupParent (0)
{
	m_pkInfo.file_attrib.attrib_info = 0;
}

#if defined(__MINGW32__)	
CfileInfo::CfileInfo(WIN32_FIND_DATAW* _data)
	: m_pkParentDir (0)
	, m_pkDupParent (0)
{
	m_pkInfo.file_attrib.attrib_info = 0;

	m_pkInfo.file_name = _data->cFileName;
	m_pkInfo.time_access = _data->ftLastAccessTime;
	m_pkInfo.time_create = _data->ftCreationTime;
	m_pkInfo.time_write = _data->ftLastWriteTime;
	m_pkInfo.file_size = SMixingNumber (_data->nFileSizeLow, _data->nFileSizeHigh);

	m_pkInfo.file_attrib.is_dir = ((_data->dwFileAttributes == 0) ||
		((_data->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY))? 1 : 0;

	m_pkInfo.file_attrib.is_file = (m_pkInfo.file_attrib.is_dir == 0) ? 1 : 0;

	m_pkInfo.file_attrib.is_system = 
		((_data->dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) == FILE_ATTRIBUTE_SYSTEM) ? 1 : 0;

	m_pkInfo.file_attrib.is_hidden = 
		((_data->dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == FILE_ATTRIBUTE_HIDDEN) ? 1 : 0;

	m_pkInfo.file_attrib.is_u_read = 1;

	m_pkInfo.file_attrib.is_u_write = 
		((_data->dwFileAttributes & FILE_ATTRIBUTE_READONLY) == FILE_ATTRIBUTE_READONLY) ? 0 : 1;
}
#else
CfileInfo::CfileInfo(dirent *_entry, const char* _current_dir)
	: m_pkParentDir (0)
	, m_pkDupParent (0)
{
	int nStatResult = -1;

	char fullpathname [PATH_MAX] = {0};
	snprintf(fullpathname, PATH_MAX, "%s/%s",_current_dir, _entry->d_name);

#if defined (__APPLE_CC__) || defined (__APPLE__)
	struct stat nStatinfo = {0};
    nStatResult = lstat(fullpathname, &nStatinfo);
#else
	struct stat64 nStatinfo = {0};
    nStatResult = stat64(fullpathname, &nStatinfo);
#endif

	if (nStatResult < 0) 
	{
		int fp = open(fullpathname, O_RDONLY);
		if (fp < 0) {
			printf("Error errno: %d file: %s\n", errno, fullpathname);
		}
		else {
#if defined (__APPLE_CC__) || defined (__APPLE__)
			nStatResult = fstat(fp, &nStatinfo);
#else
			nStatResult = fstat64(fp, &nStatinfo);
#endif
			close (fp);
			if (nStatResult < 0) {
				printf("recheck Error errno: %d file: %s\n", errno, fullpathname);
			}
		}
	}

	m_pkInfo.file_attrib.attrib_info = 0;
	m_pkInfo.file_name = _entry->d_name;
	m_pkInfo.time_access = nStatinfo.st_atime;  
	m_pkInfo.time_create = nStatinfo.st_mtime;
	m_pkInfo.time_write = nStatinfo.st_ctime;
	m_pkInfo.file_size = nStatinfo.st_size;

	m_pkInfo.file_attrib.is_dir = ((nStatinfo.st_mode & S_IFDIR) == S_IFDIR) ? 1 : 0;
	m_pkInfo.file_attrib.is_link = ((nStatinfo.st_mode & S_IFLNK) == S_IFLNK) ? 1 : 0;
	m_pkInfo.file_attrib.is_file = (m_pkInfo.file_attrib.is_dir == 0) ? 1 : 0;
	
	//get symbolic link target file
	if (m_pkInfo.file_attrib.is_link) {
		char link_path[PATH_MAX] = {0};
		if (readlink(fullpathname, link_path, PATH_MAX) == -1) {
			printf("readlink path error errno: %d file: %s\n", errno, fullpathname);
		}
		else
			m_pkInfo.symlink_target = link_path;
	}
		
	m_pkInfo.file_attrib.is_u_read = ((nStatinfo.st_mode & S_IRUSR) == S_IRUSR) ? 1 : 0;
	m_pkInfo.file_attrib.is_u_write = ((nStatinfo.st_mode & S_IWUSR) == S_IWUSR) ? 1 : 0;
	m_pkInfo.file_attrib.is_u_exec = ((nStatinfo.st_mode & S_IXUSR) == S_IXUSR) ? 1 : 0;

	m_pkInfo.file_attrib.is_g_read = ((nStatinfo.st_mode & S_IRGRP) == S_IRGRP) ? 1 : 0;
	m_pkInfo.file_attrib.is_g_write = ((nStatinfo.st_mode & S_IWGRP) == S_IWGRP) ? 1 : 0;
	m_pkInfo.file_attrib.is_g_exec = ((nStatinfo.st_mode & S_IXGRP) == S_IXGRP) ? 1 : 0;

	m_pkInfo.file_attrib.is_o_read = ((nStatinfo.st_mode & S_IROTH) == S_IROTH) ? 1 : 0;
	m_pkInfo.file_attrib.is_o_write = ((nStatinfo.st_mode & S_IWOTH) == S_IWOTH) ? 1 : 0;
	m_pkInfo.file_attrib.is_o_exec = ((nStatinfo.st_mode & S_IXOTH) == S_IXOTH)? 1 : 0;

	if ((_entry->d_type & DT_REG) == DT_REG) {
		m_pkInfo.file_attrib.is_file = 1;
	}

	if (_entry->d_name[0] == '.' && _entry->d_name[1] != '\0') {
		m_pkInfo.file_attrib.is_hidden = 1;
	}
}
#endif

CfileInfo::~CfileInfo() 
{
}
