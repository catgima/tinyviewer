#include "pch.h"
#include "stringutil.h"

#if !defined(__MINGW32__)
	#include <fnmatch.h>	
#endif

#if defined(__MINGW32__)
//soruce code from Google-glog
bool StringUtil::SafeFNMatch(const CChar* pattern,size_t patt_len,const CChar* str,size_t str_len)
{
	size_t p = 0;
	size_t s = 0;

	while (1)
	{
		if (p == patt_len && s == str_len)
			return true;
			
		if (p == patt_len)
			return false;

		if (s == str_len)
			return (p+1 == patt_len && pattern[p] == _T('*'));

		if (pattern[p] == str[s] || pattern[p] == _T('?')) {
			p += 1;
			s += 1;
			continue;
		}

		if (pattern[p] == _T('*'))
		{
			if (p+1 == patt_len) 
				return true;

			do {
				if (SafeFNMatch(pattern+(p+1), patt_len-(p+1), str+s, str_len-s)) {
					return true;
				}
				s += 1;
			} 
			while (s != str_len);
			return false;
		}
		return false;
	}
}
#endif

int StringUtil::StringMatch(const CChar *pattern, const CChar *name, int flags)
{
#if defined(__MINGW32__)
	if(SafeFNMatch (pattern,CStrlen(pattern),name,CStrlen(name)))
    	return 0;
#else
	return fnmatch(pattern, name, flags);
#endif

	return FNM_NOMATCH;
}

void StringUtil::ReverseStripString(CChar *fname, CChar _symbo)
{
    CChar *end = fname + CStrlen(fname);
    while (end > fname && *end != _symbo) {
    	--end;
    }

    if (end > fname) {
        *end = '\0';
    }
}

void StringUtil::SplitNameTag (const CStdString& _filename, std::vector<CStdString>& nList)
{
	const CChar* sepStr = _T(". ");
	CChar strbuf [256] = {0};
	const CChar* strptr = _filename.c_str();
	size_t offset = 0;
	while (*strptr) 
	{
		const CChar* ptr = sepStr;
		while (*ptr) 
		{
			if (*strptr == *ptr) 
			{
				strbuf[offset+1] = _T('\0');
				if (offset > 0) {
					nList.push_back(strbuf);
					memset (&strbuf , 0, 256);
					offset = 0;
				}
				break;
			}
			ptr++;
		}

		CChar ch = *strptr;
		if (ch != _T(' ') && ch != _T('~') && ch != _T('-') && ch != _T('_') && 
			ch != _T('[') && ch != _T(']') && ch != _T('(') && ch != _T(')'))
		{
			if (ch >= _T('A') && ch <= _T('Z')) {
				strbuf[offset] = ch | 0x20; // char | 0x20 to lowercase
			}
			else {
				strbuf[offset] = ch;
			} 
			offset++;
		}
		strptr++;
	}
}

std::wstring StringUtil::s2ws(const std::string& str)
{
    using convert_typeX = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_typeX, wchar_t> converterX;

    return converterX.from_bytes(str);
}

std::string StringUtil::ws2s(const std::wstring& wstr)
{
    using convert_typeX = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_typeX, wchar_t> converterX;

    return converterX.to_bytes(wstr);
}

bool StringUtil::CheckRegexSyntaxValid(CStdString& matchstring)
{
	if (matchstring.size() > 0) 
	{
		try {
			CStdRegex reg (matchstring.c_str());
		}
		catch (const std::regex_error& err) 
		{
			std::string error = err.what();
#if defined(__MINGW32__)			
			const CChar* errW = s2ws(error).c_str();
#else		
			const CChar* errW = error.c_str();
#endif	
			CPrintf (_T("caughted: %s\n"), err.what());

			if (err.code() == std::regex_constants::error_brack) {
				CPrintf (_T("The code gives an error_brack\n"));
			}
			return false;
		}
	}
	return true;
}

