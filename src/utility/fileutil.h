#pragma once

class ByteStream;
class FileUtil : public Singleton<FileUtil>, ObjectBase
{
public:
	FileUtil() {}
	~FileUtil() {}

	bool CreateDir (const CChar* Path);
	bool IsDirExist (const CChar* nPathString);
	bool IsFileExist (const CChar* Name);

	void StreamWriteFile (ByteStream* streamdata, const CChar* FileName);
	void StreamLoadFile (ByteStream* streamdata, const CChar* FileName);

	bool Cfopen (FILE** fileptr, const CChar* filename, const CChar* mode);
	int Csprintf (CChar* sDest, size_t len, const CChar* sFormat, ...);

	void StreamMakeMd5(FILE* fp, unsigned char* buffer);

	CStdString MakeMd5Checksum (CStdString filename);
	CStdString MakeMd5Checksum (unsigned char* buffer, size_t len);

	bool SaveFile(const CChar* filePath, CStdString& buffer);
	bool OpenFile(const CChar* filePath, std::string& buffer);

#if defined(__MINGW32__)	
	DWORD ChangeAttrib (const wchar_t* PathName, DWORD AttrValue);
#endif	
};