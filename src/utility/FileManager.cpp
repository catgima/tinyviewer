#include "pch.h"
#include "objectbase.h"
#include "MixingNumber.h"
#include "dirinfo.h"
#include "fileinfo.h"
#include "filesearch.h"
#include "FileManager.h"

FileManager::FileManager()
{
	m_File = new CFileSearch;
}

FileManager::~FileManager()
{
	delete m_File;
}

void FileManager::GetAllFileInfo(CStdString _searchPath)
{
	count = 0;

	std::vector<CStdString> nPathList;

#ifdef WIN32
	nPathList.push_back(L"E:\\BAK\\exporter_jp1");
	nPathList.push_back(L"E:\\BAK\\exporter_jp");
	nPathList.push_back(L"E:\\BAK\\exporter");
#else
	nPathList.push_back("/media/gima/HGST_1T/wd-book_a2/software/convert");
	nPathList.push_back("/media/gima/HGST_1T/wd-book_a2/software/finaldata");
	
#endif
	m_File->GetAllFilesFromPath(nPathList);
}

void FileManager::CreateFileList()
{
	std::vector<SDir*>& nRootDirs = m_File->GetRootDir();
	std::vector<SDir*>::iterator it = nRootDirs.begin();
	for (; it != nRootDirs.end(); ++it) {
		GetDirInfoRecursive(*it);
	}
	printf("total count :%d\n", count);
}

void FileManager::GetDirInfoRecursive(SDir* _nDir)
{
	if (!_nDir)
		return;

	//m_File->ShowAttribString(_nDir->pDirInfo, _nDir);
	std::vector<SDir*>::iterator dir_it = _nDir->SubDirList.begin();
	for (; dir_it != _nDir->SubDirList.end(); ++dir_it) {
		GetDirInfoRecursive(*dir_it);
	}

	std::vector<CfileInfo*>::iterator file_it = _nDir->FileList.begin();
	for (; file_it != _nDir->FileList.end(); ++file_it) {
		m_File->ShowAttribString(*file_it, _nDir);
		count++;
	}
}

unsigned int FileManager::GetNameTableCount()
{
	return (unsigned int)CDirinfo::NameMapTable.size();
}

unsigned int FileManager::GetDirTableCount()
{
	return (unsigned int)CDirinfo::DirNameTable.size();
}