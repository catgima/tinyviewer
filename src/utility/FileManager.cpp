#include "pch.h"
#include <algorithm>
#include "MixingNumber.h"
#include "stringutil.h"
#include "dirinfo.h"
#include "fileinfo.h"
#include "filesearch.h"
#include "fileutil.h"
#include "pathutil.h"
#include "stringutil.h"
#include "yyjson.h"
#include "FileManager.h"

struct FilelistSortCompare
{
    inline bool operator() (CfileInfo* s1, CfileInfo* s2) {
        return (s1->FILE_NAME() < s2->FILE_NAME());
    }
};

FileManager::FileManager()
	: m_pkMutDoc (0)
	, m_pkMutRoot (0)
{
	m_File = new CFileSearch;
	m_Dir = m_File->GetDirManager();

	m_pkFileUtil = &FileUtil::instance();
	pkStringUtil = &StringUtil::instance();
}

FileManager::~FileManager()
{
	delete m_File;
}

void FileManager::GetAllFileInfo(SCommandInfo* _info)
{
	std::vector<SPathFileInfo>::iterator it = _info->findlist.begin();
	for (; it != _info->findlist.end(); ++it) 
	{
		CStdString nDirString = CStdString(it->drv) + CStdString(it->dir);
		CStdString nFileNameFilter = CStdString(it->file) + CStdString(it->ext);
		
		_info->rootdir = m_File->SearchDir(nDirString.c_str(), &_info->flag);
	}
}

bool FileManager::GetFolderInfo(SCommandInfo* _filterinfo, CStdString _path, SFolderInfo& _info)
{
	_info.ClearAll();

	SDir* dirinfo = m_Dir->GetDir(_path);
	if (dirinfo) {
		//GetDirInfoRecursive(_filterinfo, dirinfo, &_info);
		return true;
	}
	return false;
}

void FileManager::CreateFileList(SCommandInfo* _info)
{
	mkDupCheck.Clear();
	SortFilelist.clear();

	SFolderInfo dirinfo;
	std::list<SDir*>& nRootDirs = m_File->GetRootDir();
	std::list<SDir*>::iterator it = nRootDirs.begin();

	if (_info->flag.sort) 
	{
		FileManager::OnDirFileInfoOutputStart (_info, &dirinfo);
		for (; it != nRootDirs.end(); ++it) {
			GetDirInfoRecursive(_info, *it, &dirinfo);
		}
		FileManager::OnDirFileInfoOutputEnd (_info, &dirinfo);
		OnDirFileInfoOutputEnd(_info, &dirinfo);
	}
	else
	{
		OnDirFileInfoOutputStart (_info, &dirinfo);
		for (; it != nRootDirs.end(); ++it) {
			GetDirInfoRecursive(_info, *it, &dirinfo);
		}
		OnDirFileInfoOutputEnd (_info, &dirinfo);
	}
}

void FileManager::OnFileInfoOutput(SCommandInfo *_filterinfo, CfileInfo *_info)
{
	if (_info->IsDir() && _filterinfo->flag.show_file_only)
		return;

	if (_info->IsFile() && _filterinfo->flag.show_dir_only)
		return;

	if (_info->IsHidden() && _filterinfo->flag.show_full_info == 0)
		return;
	
	if (_filterinfo->flag.use_match_search != 0 && !CheckMatchString (_filterinfo, _info->FILE_NAME())) 
		return;
	
	if (_filterinfo->flag.duptag_check) {
		CheckDupTagName (_info);
	}

	if (_filterinfo->flag.sort) {
		SortFilelist.push_back (_info);
	}

	if (_filterinfo->flag.write_db) 
	{
		CStdString ParentPath = m_Dir->GetPath(_info->GetParentDir());

#if __MINGW32__	
		const std::string Path = WCHARtoCHAR(ParentPath);
		const std::string Name = WCHARtoCHAR(_info->FILE_NAME());
#else
		const std::string Path = ParentPath;
		const std::string Name = _info->FILE_NAME();
#endif


		yyjson_mut_val* nDirArray = yyjson_mut_arr(m_pkMutDoc);
		yyjson_mut_val *val1 = yyjson_mut_strcpy (m_pkMutDoc, Name.c_str());
		yyjson_mut_arr_append (nDirArray, val1);

		yyjson_mut_val *val2 = yyjson_mut_uint (m_pkMutDoc, _info->FILE_SIZE());
		yyjson_mut_arr_append (nDirArray, val2);

		char* key = unsafe_yyjson_mut_strncpy(m_pkMutDoc, Path.c_str(), Path.size());

		yyjson_mut_val* found = yyjson_mut_obj_get (m_pkMutRoot, Path.c_str());
		if (!found) {
			yyjson_mut_val* nParentArr = yyjson_mut_arr(m_pkMutDoc);
			yyjson_mut_obj_add_val (m_pkMutDoc, m_pkMutRoot, key, nParentArr);	
			yyjson_mut_val *one = yyjson_mut_arr_add_obj (m_pkMutDoc, nParentArr);
			yyjson_mut_obj_add_val (m_pkMutDoc, one, key, nDirArray);	

		}
		else {
			/*
			yyjson_mut_val* nParentArr = yyjson_mut_arr(m_pkMutDoc);
			yyjson_mut_obj_add_val (m_pkMutDoc, m_pkMutRoot, key, nParentArr);	
			yyjson_mut_arr_add_arr(m_pkMutDoc, nDirArray);
			*/
		}
	

/*
		yyjson_mut_val* nDirObj = yyjson_mut_arr(m_pkMutDoc);
		yyjson_mut_val *val1 = yyjson_mut_strcpy (m_pkMutDoc, Name.c_str());
		yyjson_mut_arr_append (nDirObj, val1);

		yyjson_mut_val *val2 = yyjson_mut_uint (m_pkMutDoc, _info->FILE_SIZE());
		yyjson_mut_arr_append (nDirObj, val2);
		
		char* key = unsafe_yyjson_mut_strncpy(m_pkMutDoc, Path.c_str(), Path.size());
		yyjson_mut_obj_add_val (m_pkMutDoc, m_pkMutRoot, key, nDirObj);
*/	
	}
}

void FileManager::OnDirFileInfoOutputStart (SCommandInfo *_filterinfo, SFolderInfo* _info) 
{
	if (_filterinfo->flag.write_db) {
		m_pkMutDoc = yyjson_mut_doc_new(NULL);
		m_pkMutRoot = yyjson_mut_obj(m_pkMutDoc);
		yyjson_mut_doc_set_root(m_pkMutDoc, m_pkMutRoot);
	}
}

void FileManager::OnDirFileInfoOutputEnd (SCommandInfo *_filterinfo, SFolderInfo* _info) 
{
	if (_filterinfo->flag.duptag_check) {
		std::vector<CfileInfo*>::iterator it = mkDupCheck.DupTagNameList.begin();
		for (; it != mkDupCheck.DupTagNameList.end(); ++it) {
			OnFileInfoPrint(_filterinfo, *it);
		}
	}
	else {
		std::sort (SortFilelist.begin(), SortFilelist.end(), FilelistSortCompare());
		std::vector<CfileInfo*>::iterator it = SortFilelist.begin();
		for (; it != SortFilelist.end(); ++it) {
			OnFileInfoPrint(_filterinfo, *it);
		}
	}

	if (_filterinfo->flag.write_db) 
	{
		yyjson_write_flag flg = YYJSON_WRITE_PRETTY;
		std::string json = yyjson_mut_write (m_pkMutDoc, flg, NULL);

	#if __MINGW32__
		std::wstring nJsonData = CHARtoWCHAR (json);
		m_pkFileUtil->SaveFile (_T("e:/test.json"), nJsonData);
	#else
		m_pkFileUtil->SaveFile (_T("test.json"), json);
	#endif
		yyjson_mut_doc_free (m_pkMutDoc);
	}
}

void FileManager::GetDirInfoRecursive(SCommandInfo* _filterinfo, SDir* _nDir, SFolderInfo* _info)
{
	if (!_nDir)
		return;

	if (_nDir != _filterinfo->rootdir) {
		if (_filterinfo->flag.sort)
			FileManager::OnFileInfoOutput(_filterinfo, _nDir->pDirInfo);
		else
			OnFileInfoOutput(_filterinfo, _nDir->pDirInfo);
	}

	std::list<SDir*>::iterator dir_it = _nDir->SubDirList.begin();
	for (; dir_it != _nDir->SubDirList.end(); ++dir_it) {
		GetDirInfoRecursive(_filterinfo, *dir_it, _info);
	}

	if (_info)  {
		_info->dircount += _nDir->SubDirList.size();
	}

	std::list<CfileInfo*>::iterator file_it = _nDir->FileList.begin();
	for (; file_it != _nDir->FileList.end(); ++file_it) 
	{
		CfileInfo* obj = (*file_it);

		if (_filterinfo->flag.sort)
			FileManager::OnFileInfoOutput(_filterinfo, obj);
		else
			OnFileInfoOutput(_filterinfo, obj);
		
		if (obj->IsFile()) {
			if (_info) {
				_info->filecount++;
				_info->totalsize += obj->FILE_SIZE();
			}
		}
	}
}

bool FileManager::CheckMatchString (SCommandInfo* _filterinfo, const CStdString& filename)
{
	if (_filterinfo->flag.use_match_search == 2) 
	{
		CStdString nlowcase_name = filename;
		std::transform (nlowcase_name.begin(), nlowcase_name.end(), nlowcase_name.begin(), CToLower);

		std::vector<CStdString>::iterator it = _filterinfo->macth_string.begin();
		for (; it != _filterinfo->macth_string.end(); ++it)
		{
			const CChar* nCheckName = (_filterinfo->flag.use_match_search == 2) ?
				nlowcase_name.c_str() : filename.c_str();

			int ret = pkStringUtil->StringMatch (it->c_str(), nCheckName, 
				FNM_PATHNAME|FNM_PERIOD
			);
			
			if (ret == FNM_NOMATCH) {
				return false;
			}
		}
	}
	else if (_filterinfo->flag.use_match_search == 3 || _filterinfo->flag.use_match_search == 4) 
	{
		std::vector<CStdString>::iterator it = _filterinfo->macth_string.begin();
		for (; it != _filterinfo->macth_string.end(); ++it)
		{
			CStdSmatch nMatchString;
			CStdRegex nRegex (it->c_str());

			if (_filterinfo->flag.use_match_search == 3) {
				if (!std::regex_match(filename, nMatchString, nRegex)) {
					return false;
				}
			}
			else if (_filterinfo->flag.use_match_search == 4) {
				if (!std::regex_search(filename, nMatchString, nRegex)) {
					return false;
				}
			}
#if false
			for (int i=0; i<nMatchString.size(); i++) {
				std::wstring match_substring = nMatchString[i];
				CPrintf(_T("\t\t-----> res[%2d]: %ls\n"), i, match_substring.c_str());
			}
#endif			
		}
	}
	return true;
}

void FileManager::CheckDupTagName(CfileInfo* _fileinfo)
{
	std::vector<CStdString> TagList;
	pkStringUtil->SplitNameTag (_fileinfo->FILE_NAME(), TagList);

	CStdString TagName;

	if (TagList.size() >= 2) 
	{
		CChar ch = TagList[1][0];
		if (ch >= _T('0') && ch <= _T('9') && ch != 0x20 && TagList[1].size() > 2)
			TagName = TagList[0] + TagList[1];
		else
			TagName = TagList[0];
	}
	else {
		if (TagList.size()==1) {
			CChar ch = TagList[0][0];
			if ((ch >= _T('0') && ch <= _T('9')) || (ch >= _T('a') && ch <= _T('z'))) 
				TagName = TagList[0];
		}
	}

	if (TagName.size() == 0)
		TagName = _fileinfo->FILE_NAME();

	std::map<CStdString, CfileInfo*>::iterator it = mkDupCheck.TagNameTable.find(TagName);
	if (it == mkDupCheck.TagNameTable.end()) {
		mkDupCheck.TagNameTable.insert (std::make_pair(TagName, _fileinfo));
	}
	else {
		_fileinfo->SetDupParent(it->second);
		mkDupCheck.DupTagNameList.push_back(_fileinfo);
	}
}

