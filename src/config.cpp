#include "pch.h"
#include <locale>
#include <codecvt>
#include <string>
#include "pathutil.h"
#include "fileutil.h"
#include "yyjson.h"
#include "config.h"

ConfigMgr::ConfigMgr()
	: m_pkDoc(0)
	, m_pkRoot(0)
{
}

ConfigMgr::~ConfigMgr()
{
	if (m_pkDoc) {
		yyjson_doc_free (m_pkDoc);
	}
}

void ConfigMgr::ReadSearchPathList()
{
	const CChar* FilePathName = _T("config.json");

	FILE* fp;
	if (!FileUtil::instance().Cfopen (&fp, FilePathName, _T("rb"))) {
	#if __MINGW32__
		CPrintf(_T("Current Dir: %ls\n"), PathUtil::instance().GetWorkPath().c_str());
		CPrintf(_T("file not found: %ls\n"), FilePathName);
	#else
		CPrintf(_T("Current Dir: %s\n"), PathUtil::instance().GetWorkPath().c_str());
		CPrintf(_T("file not found: %s\n"), FilePathName);
	#endif
		return;
	}

	fseek (fp, 0, SEEK_END);
	size_t nfileSize = ftell (fp);
	fseek (fp, 0, SEEK_SET);

	char* nBuffer = new char [nfileSize];
	fread (nBuffer, nfileSize, 1, fp);
	fclose (fp);
	
	// Read JSON and get root
	yyjson_read_flag nFlag = YYJSON_READ_ALLOW_COMMENTS | YYJSON_READ_ALLOW_TRAILING_COMMAS;
	m_pkDoc = yyjson_read (nBuffer, nfileSize, nFlag);
	m_pkRoot = yyjson_doc_get_root (m_pkDoc);

	delete [] nBuffer;
}

void ConfigMgr::GetAllPath(std::vector<CStdString>& nArgList)
{
	nArgList.clear();

#if __MINGW32__
	yyjson_val* paths = yyjson_obj_get (m_pkRoot, "SearchPath1");
#else	
	yyjson_val* paths = yyjson_obj_get (m_pkRoot, "SearchPath2");
#endif

	size_t idx, max;
	yyjson_val* path;
	yyjson_arr_foreach (paths, idx, max, path) 
	{
		CChar* nPath;
#if __MINGW32__
		nPath = (CChar*)CHARtoWCHAR (yyjson_get_str (path)).c_str();
#else
		nPath = (CChar*)yyjson_get_str (path);
#endif
		if (nPath[0] == _T('/') && nPath[1] == _T('/'))
			continue;

		nArgList.push_back(nPath);
	}
}	

void ConfigMgr::WriteSearchPathList()
{
	
	yyjson_mut_doc *doc = yyjson_mut_doc_new(NULL);
	yyjson_mut_val *root = yyjson_mut_obj(doc);
	yyjson_mut_doc_set_root(doc, root);

	// Set root["name"] and root["star"]
	yyjson_mut_obj_add_str(doc, root, "name", "Mash");
	yyjson_mut_obj_add_int(doc, root, "star", 4);

	// Set root["hits"] with an array
	int hits_arr[] = {2, 2, 1, 3};
	yyjson_mut_val *hits = yyjson_mut_arr_with_sint32(doc, hits_arr, 4);
	yyjson_mut_obj_add_val(doc, root, "hits", hits);

	yyjson_write_flag nFlag = YYJSON_WRITE_PRETTY;

	// To string, minified
	const char *json = yyjson_mut_write(doc, nFlag, NULL);
	if (json) {
		printf("json: %s\n", json); // {"name":"Mash","star":4,"hits":[2,2,1,3]}
		free((void *)json);
	}

	// Free the doc
	yyjson_mut_doc_free(doc);
	
}