#include "pch.h"
#if defined(__MINGW32__)
#include <direct.h>
#endif

#include <wchar.h>

//#include "thread_pool.hpp"
#include "MixingNumber.h"
#include "fileinfo.h"
#include "filesearch.h"
#include "stringutil.h"
#include "pathutil.h"
#include "config.h"
#include "dirinfo.h"
#include "fileutil.h"
#include "FileManager.h"
#include "console.h"

ConsoleCommand::ConsoleCommand()
	: FileManager()
{
	pkPathUtil = &PathUtil::instance();
	pkStringUtil = &StringUtil::instance();
}

ConsoleCommand::~ConsoleCommand()
{
}

void ConsoleCommand::ReadCommand(int count, CChar *args[])
{
	printf("command count:%d\n", count);

	std::vector<CStdString> nArgList;
#if defined(__MINGW32__)
	StrSplit<CStdString>(args[0], L" ", nArgList);
#else
	for (int i = 1; i < count; ++i)
	{
		nArgList.push_back(args[i]);
	}
#endif

	if (nArgList.size() < 1)
	{
		PrintHelp();
		return;
	}

	SCommandInfo nCommandInfo;
	std::vector<CStdString>::iterator it = nArgList.begin();
	for (; it != nArgList.end(); ++it)
	{
		if (nCommandInfo.flag.use_match_search && it->size() > 0) {
			CStdString matchstring = _T("*") + *it + _T("*");
			std::transform (matchstring.begin(), matchstring.end(), matchstring.begin(), CToLower);
			nCommandInfo.macth_string.push_back (std::move (matchstring));
			continue;
		}

		if (!ParseCommand(*it, nCommandInfo))
			return;
	}

	if (nCommandInfo.flag.make_file_list)
	{
		nCommandInfo.findlist.clear();
		ConfigMgr::instance().GetAllPath(nArgList);
		std::vector<CStdString>::iterator it = nArgList.begin();
		for (; it != nArgList.end(); ++it) {
			if (!ParseCommand(*it, nCommandInfo))
				return;
		}
	}

	ExecCommand (&nCommandInfo);
}

bool ConsoleCommand::ParseCommand(CStdString &commandstring, SCommandInfo &_info)
{
	if (commandstring.size() == 0)
		return true;

	bool result = false;
	size_t token = 0;
	size_t command_length = commandstring.size();

	if (command_length > 0 && commandstring.at(0) == _T('-'))
	{
		for (size_t i = 1; i < command_length; ++i)
		{
			switch (commandstring.at(i))
			{
			case _T('r'): // recursive search
			{
				std::size_t found_m = commandstring.find(_T("rmatch="));
				if (found_m != CStdString::npos) {
					_info.flag.use_match_search = 3;
					CStdString matchstring = commandstring.substr(i+7, command_length);
					if (!pkStringUtil->CheckRegexSyntaxValid(matchstring)) 
						return false;

					_info.macth_string.push_back (std::move (matchstring));
					return true;
				}

				std::size_t found_s = commandstring.find(_T("rsearch="));
				if (found_s != CStdString::npos) {
					_info.flag.use_match_search = 4;
					CStdString matchstring = commandstring.substr(i+8, command_length);
					if (!pkStringUtil->CheckRegexSyntaxValid(matchstring)) 
						return false;

					_info.macth_string.push_back (std::move (matchstring));
					return true;
				}
				_info.flag.recursive = 1;
				break;
			}
			case _T('a'): // print full infomation
				_info.flag.show_full_info = 1;
				break;

			case _T('s'): // sort file
				_info.flag.sort = 1;
				break;

			case _T('S'): // search match file
			{
				_info.flag.use_match_search = 2;

				CStdString matchstring = commandstring.substr(i+1, command_length);
				if (matchstring.size() > 0) 		
				{
					std::transform (matchstring.begin(), matchstring.end(), matchstring.begin(), CToLower);
					matchstring = _T("*") + matchstring + _T("*");
					_info.macth_string.push_back (std::move (matchstring));
				}
				return true;
			}
			case _T('R'): // write db
				if (command_length > 3) {
					_info.flag.read_db = 1;
					_info.db_name = commandstring.substr(2, command_length);
				}
				return true;

			case _T('W'): // read db
				if (command_length > 3) {
					_info.flag.write_db = 1;
					_info.db_name = commandstring.substr(2, command_length);
				}
				return true;

			case _T('D'): // duplicate file name tag check
				_info.flag.duptag_check = 1;
				break;

			case _T('d'): // print directory information only
				_info.flag.show_dir_only = 1;
				break;

			case _T('f'): // print file information only
				_info.flag.show_file_only = 1;
				break;

			case _T('p'): // print path information
				_info.flag.show_path = 1;
				break;

			case _T('m'): // print file information only
				_info.flag.make_file_list = 1;
				break;

			case _T('K'): // print file size unit use Kilobyte
				_info.flag.show_size_unit = 1;
				break;

			case _T('M'): // print file size unit use Gigabyte
				_info.flag.show_size_unit = 2;
				break;

			case _T('G'): // print file size unit use Megabyte
				_info.flag.show_size_unit = 3;
				break;

			case _T('H'):
			case _T('h'): // print file size unit use Megabyte
				PrintHelp();
				break;

			default: 
				CPrintf(_T("input command arg -%c not define\n"), commandstring.at(i));
				return false;
			}
		}
	}
	else
	{
		if (commandstring[1] == _T('/') || commandstring[1] == _T('\\'))
		{
			if (commandstring[0] == _T('.')) {
				commandstring = pkPathUtil->GetCurrentFullPath() + commandstring.erase(0, 1);
			}
			else if (commandstring[0] == _T('~')) {
				commandstring = _T("/home/") + pkPathUtil->getUserDir() + commandstring.erase(0, 1);
			}
		}

		///run/user/1000/gvfs/mtp:host=SAMSUNG_SAMSUNG_Android_RF8M93C6VKN/內部儲存空間/ADM
		
		SPathFileInfo nPathSplitinfo = {0};

		std::size_t found = commandstring.find("mtp:host=");
  		if (found!=std::string::npos) {
			nPathSplitinfo.IsDir = true;
			CStrncpy (nPathSplitinfo.dir, &commandstring[0], commandstring.size());
		}
		else {
			pkPathUtil->SplitPath(commandstring.c_str(), nPathSplitinfo);
		}

		if (!nPathSplitinfo.IsDir) 
		{
			if (_info.flag.make_file_list) {
				#if defined(__MINGW32__)
					CPrintf(_T("Folder not found: %ls\n"), commandstring.c_str());
				#else
					CPrintf(_T("Folder not found: %s\n"), commandstring.c_str());
				#endif
				return false;
			}
			
			_info.flag.recursive = 0;
			_info.flag.use_match_search = 2;

				CStdString matchstring = nPathSplitinfo.file+ (CStdString)nPathSplitinfo.ext;
				if (matchstring.size() > 0) 		
				{
					std::transform (matchstring.begin(), matchstring.end(), matchstring.begin(), CToLower);
					_info.macth_string.push_back (std::move (matchstring));
				}
		}

		_info.findlist.push_back(nPathSplitinfo);
	}

	return true;
}

void ConsoleCommand::ExecCommand(SCommandInfo *info)
{
	GetAllFileInfo(info);
	CreateFileList(info);
}

void ConsoleCommand::OnDirFileInfoOutputStart(SCommandInfo *_filterinfo, SFolderInfo *_info)
{
}

void ConsoleCommand::OnDirFileInfoOutputEnd(SCommandInfo *_filterinfo, SFolderInfo *_info)
{
	CChar nFileSizeText[32] = {0};
	GetFileSizeUnitText(_info->totalsize, nFileSizeText, 32);

#if defined(__MINGW32__)
	CPrintf(_T("\n\ndircount:%u filecount:%u totalsize:%ls\n"),
			_info->dircount, _info->filecount, nFileSizeText);
#else
	CPrintf(_T("\n\ndircount:%u filecount:%u totalsize:%s\n"),
			_info->dircount, _info->filecount, nFileSizeText);
#endif
}

void ConsoleCommand::OnFileInfoOutput(SCommandInfo *_filterinfo, CfileInfo *_info)
{
	if (_info->IsDir() && _filterinfo->flag.show_file_only)
		return;

	if (_info->IsFile() && _filterinfo->flag.show_dir_only)
		return;

	if (_info->IsHidden() && _filterinfo->flag.show_full_info == 0)
		return;

	if (_filterinfo->flag.use_match_search && !CheckMatchString (_filterinfo, _info->FILE_NAME()))
	{
		return;
	}

	if (_filterinfo->flag.duptag_check)
	{
		CheckDupTagName(_info);
	}
	else
	{
		OnFileInfoPrint(_filterinfo, _info);
	}
}

void ConsoleCommand::OnFileInfoPrint(SCommandInfo *_filterinfo, CfileInfo *_info)
{
	CChar AttribString[10] = {0};
	CChar StatusString[4] = {0};

	if (_filterinfo->flag.show_full_info)
	{
		int idx = 0;
		if (_info->IsDir())
			StatusString[idx++] = _T('d');
		else
			StatusString[idx++] = _T('-');

		if (_info->IsHidden())
			StatusString[idx++] = _T('h');

		if (_info->IsLink())
			StatusString[idx++] = _T('l');

		StatusString[idx] = _T('\0');

		unsigned short nAllAttrib = _info->GetAllFilePermission();

		int i = 0;
		for (; i < 9; i++)
		{
			if ((nAllAttrib & (1 << i)) == 0)
			{
				AttribString[i] = _T('-');
				continue;
			}

			int res = i % 3;
			switch (res)
			{
			case 0:
				AttribString[i] = _T('r');
				break;
			case 1:
				AttribString[i] = _T('w');
				break;
			case 2:
				AttribString[i] = _T('x');
				break;
			defalut:
				break;
			}
		}
	}

	CChar nFileSizeText[128] = {0};

	if (_filterinfo->flag.show_size_unit == 1)
	{
		m_pkFileUtil->Csprintf(nFileSizeText, 128, _T("%10.1f KB"), (float)_info->FILE_SIZE() / 1024);
	}
	else if (_filterinfo->flag.show_size_unit == 2)
	{
		m_pkFileUtil->Csprintf(nFileSizeText, 128, _T("%10.1f MB"), (float)_info->FILE_SIZE() / 1048576);
	}
	else if (_filterinfo->flag.show_size_unit == 3)
	{
		m_pkFileUtil->Csprintf(nFileSizeText, 128, _T("%10.1f GB"), (float)_info->FILE_SIZE() / 1073741824);
	}
	else
	{
		GetFileSizeUnitText(_info->FILE_SIZE(), nFileSizeText, 128);
	}

	CStdString ParentPath = m_Dir->GetPath(_info->GetParentDir());

	CStdString DupParentPath;
	if (_filterinfo->flag.duptag_check) {
		CfileInfo* obj = _info->GetDupParent();
		if (obj) {
			DupParentPath = m_Dir->GetPath(obj->GetParentDir()) + _T("/") + obj->FILE_NAME();
		}
	}

	CChar nFileInfoText[1024] = {0};

#if !defined(__MINGW32__)
	if (_info->IsLink())
	{
		m_pkFileUtil->Csprintf(nFileInfoText, 1024, 
			"\x1b[32m %-3s %s\t %s \t %s/%s ---> %s\x1b[37m\n",
			StatusString, AttribString,
			nFileSizeText, ParentPath.c_str(), _info->FILE_NAME().c_str(), _info->LINK_TARGET_NAME().c_str());
	}
	else if (_info->IsFile())
	{
		if (_filterinfo->flag.show_full_info || _filterinfo->flag.show_path)
		{
			if (DupParentPath.size() > 0)
			{
				m_pkFileUtil->Csprintf(nFileInfoText, 1024, " %s \t %s/%s \n\t--> %s\n\n", 
					nFileSizeText, ParentPath.c_str(), _info->FILE_NAME().c_str(), DupParentPath.c_str());
			}
			else
			{
				if (_filterinfo->flag.show_full_info) {
					m_pkFileUtil->Csprintf(nFileInfoText, 1024, " %-3s %s\t %s \t %s/%s\n", StatusString, AttribString,
						nFileSizeText, ParentPath.c_str(), _info->FILE_NAME().c_str());
				}
				else {
					m_pkFileUtil->Csprintf(nFileInfoText, 1024, " %s \t %s/%s\n",  
						nFileSizeText, ParentPath.c_str(), _info->FILE_NAME().c_str());
				}
			}
		}
		else
		{
			m_pkFileUtil->Csprintf(nFileInfoText, 1024, " %s \t %s\n",
				nFileSizeText, _info->FILE_NAME().c_str());
		}
	}
	else
	{
		m_pkFileUtil->Csprintf(nFileInfoText, 1024, "\x1B[0;36m %-3s %s\t %s \t %s\n",
			StatusString, AttribString, _T("\t<DIR>"), ParentPath.c_str());
	}
#else
	if (_info->IsFile())
	{
		if (_filterinfo->flag.show_full_info || _filterinfo->flag.show_path)
		{
			if (DupParentPath.size() > 0)
			{
				m_pkFileUtil->Csprintf(nFileInfoText, 1024, _T(" %ls \t %ls/%ls --> %ls\n"), 
					nFileSizeText, ParentPath.c_str(), _info->FILE_NAME().c_str(), DupParentPath.c_str());
			}
			else
			{
				if (_filterinfo->flag.show_full_info) {
					m_pkFileUtil->Csprintf(nFileInfoText, 1024, _T(" %-l3s %ls\t %ls \t %ls/%ls\n"), StatusString, AttribString,
						nFileSizeText, ParentPath.c_str(), _info->FILE_NAME().c_str());
				}
				else {
					m_pkFileUtil->Csprintf(nFileInfoText, 1024, _T(" %ls \t %ls/%ls\n"),  
						nFileSizeText, ParentPath.c_str(), _info->FILE_NAME().c_str());
				}
			}
		}
		else
		{
			m_pkFileUtil->Csprintf(nFileInfoText, 1024, _T(" %ls \t %ls\n"),
				nFileSizeText, _info->FILE_NAME().c_str());
		}
	}
	else
	{
		m_pkFileUtil->Csprintf(nFileInfoText, 1024, _T(" %-3ls %ls\t %ls \t %ls\n"), StatusString, AttribString,
			_T("\t<DIR>"), ParentPath.c_str());
	}
#endif

#if defined(__MINGW32__)
	CPrintf(_T("%ls"), nFileInfoText);
#else
	CPrintf(_T("%s"), nFileInfoText);
#endif
}

void ConsoleCommand::GetFileSizeUnitText(uint64_t _size, CChar *nFileSize, int _textlen)
{
	if ((_size >> 40) > 0)
	{
		m_pkFileUtil->Csprintf(nFileSize, _textlen, _T("%-5.1f TB"), (float)_size / 1099511627776);
	}
	else if ((_size >> 30) > 0)
	{
		m_pkFileUtil->Csprintf(nFileSize, _textlen, _T("%-5.1f GB"), (float)_size / 1073741824);
	}
	else if ((_size >> 20) > 0)
	{
		m_pkFileUtil->Csprintf(nFileSize, _textlen, _T("%-5.1f MB"), (float)_size / 1048576);
	}
	else if ((_size >> 10) > 0)
	{
		m_pkFileUtil->Csprintf(nFileSize, _textlen, _T("%-5.1f KB"), (float)_size / 1024);
	}
	else
	{
#if defined(__MINGW32__) || defined(__APPLE_CC__) || defined(__APPLE__)
		m_pkFileUtil->Csprintf(nFileSize, _textlen, _T("%-5llu Bytes "), _size);
#else
		m_pkFileUtil->Csprintf(nFileSize, _textlen, _T("%-5lu Bytes "), _size);
#endif
	}
}

void ConsoleCommand::PrintHelp()
{
	CPrintf(_T("\n Using: ftool -args path \n\n"));
	CPrintf(_T(" -r recursive search\n"));
	CPrintf(_T(" -a print full infomation\n"));
	CPrintf(_T(" -s list file name sort\n"));
	CPrintf(_T(" -d only print directory information\n"));
	CPrintf(_T(" -f only print file information\n"));
	CPrintf(_T(" -p print path information\n"));
	CPrintf(_T(" -m list file from config.json specified path\n"));
	CPrintf(_T(" -K print file size unit use Kilobyte\n"));
	CPrintf(_T(" -M print file size unit use Gigabyte\n"));
	CPrintf(_T(" -G print file size unit use Megabyte\n"));
	CPrintf(_T(" -D duplicate file name tag check\n"));
	CPrintf(_T(" -S search match file\n"));
	CPrintf(_T(" -R read file db\n"));
	CPrintf(_T(" -W write file db\n"));
	CPrintf(_T(" -rmatch filter string for regex match\n"));
	CPrintf(_T(" -rsearch filter string for regex search\n"));
	
	CPrintf(_T(" -v Version : 2021.10.12 \n"));
}
