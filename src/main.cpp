#include "pch.h"

#if defined(__MINGW32__)
    #include <shellapi.h>
#endif

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "objectbase.h"
#include "MixingNumber.h"
#include "fileinfo.h"
#include "filesearch.h"
#include "FileManager.h"


#if defined(__MINGW32__)
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR lpCmdLine, int nCmdShow)
#else
int main(int argc, char** lpCmdLine)
#endif
{
    struct SRes {
        double cpu;
        double prog;
    };
    
    setlocale(LC_CTYPE, "");
    std::cout << "LC_ALL: " << setlocale(LC_ALL, NULL) << std::endl;
    std::cout << "LC_CTYPE: " << setlocale(LC_CTYPE, NULL) << std::endl;

    std::vector<CStdString> nArgList;
#if defined(__MINGW32__)
    printf("command count:%d\n", nCmdShow);
    StrSplit<CStdString> (lpCmdLine, L" ", nArgList);
#else
    printf("command count:%d\n", argc);
    for (int i=1; i<argc; ++i) {
		nArgList.push_back (lpCmdLine[i]);
	}    
#endif    

    int cmdcount = (int)nArgList.size();
    
    if (nArgList.size() <1)
    {
#if defined(__MINGW32__)
        nArgList.push_back(L"E:\\");
#else
        nArgList.push_back("/Volumes/WD-1T/mov");
        //nArgList.push_back("/Volumes/");
#endif
    }

    //MessageBoxW (NULL, L"Hello World!", L"hello", MB_OK | MB_ICONINFORMATION); 

    //check this program is 32 or 64bit
    void* number =  0;
    int size = sizeof(&number);
    if (size==4) {
        printf("32 bit:%d\n", size);
    }else {
	    printf("64 bit:%d\n", size);
    }

    
    FileManager nFileMgr;
    nFileMgr.GetAllFileInfo(nArgList[0]);
    nFileMgr.CreateFileList();
    
    SRes result;
    //s.GetAllFilesFromPath(nArgList); 

    result.cpu = nFileMgr.GetTimeSpendFromCPU();
    result.prog = nFileMgr.GetTimeSpendFromProg();

    setlocale(LC_ALL, "");
    
    wprintf (L"_________________________________________________\n");
    wprintf (L"程式執行間隔時間：%f\n", result.cpu);
    wprintf (L"進行運算所花費的時間：：%f\n", result.prog);
    wprintf (L"Name TAble count：：%u\n", nFileMgr.GetNameTableCount());
    wprintf (L"Dir TAble count：：%u\n", nFileMgr.GetDirTableCount());
 
    printf ("command count = %d\n", cmdcount);
    std::vector<CStdString>::iterator nPathIt = nArgList.begin();
	for (; nPathIt != nArgList.end(); ++nPathIt) {
#if defined(__MINGW32__)        
        printf("%ls\n", nPathIt->c_str());
#else
        printf("%s\n", nPathIt->c_str());
#endif        
	}
    
    return 0;
} 