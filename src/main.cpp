#include "pch.h"
#include "systeminfo.h"
#include "config.h"
#include "pathutil.h"
#include "filesearch.h"
#include "FileManager.h"
#include "console.h"

#if defined(__MINGW32__)
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR lpCmdLine, int nCmdShow)
#else
int main(int argc, char** lpCmdLine)
#endif
{
    SystemInfo::instance().Init();
    ConfigMgr::instance().ReadSearchPathList();

    //ConfigMgr::instance().WriteSearchPathList();

    ConsoleCommand textcommand;
#if defined(__MINGW32__)    
    textcommand.ReadCommand(0, &lpCmdLine);
#else
    textcommand.ReadCommand(argc, lpCmdLine);
#endif    
    return 0;
} 