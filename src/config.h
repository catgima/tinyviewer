#pragma once

struct yyjson_doc;
struct yyjson_val;
class ConfigMgr : public Singleton<ConfigMgr>, ObjectBase
{
	yyjson_doc* m_pkDoc;
	yyjson_val* m_pkRoot;
	
public:	
	ConfigMgr();
	~ConfigMgr();

	void ReadSearchPathList();
	void WriteSearchPathList();

	void GetAllPath(std::vector<CStdString>& nArgList);
};