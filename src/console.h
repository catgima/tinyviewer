#pragma once

struct SCommandInfo;
class PathUtil;
class CfileInfo;
class StringUtil;
struct SDir;

class ConsoleCommand : public FileManager
{
	PathUtil* pkPathUtil;

	bool ParseCommand(CStdString& commandstring, SCommandInfo& _info);
	void GetFileSizeUnitText (uint64_t _size, CChar* _Text, int _textlen);
	void PrintHelp();

public:
	ConsoleCommand();
	~ConsoleCommand();

	void ReadCommand(int count, CChar* args[]);
	void ExecCommand(SCommandInfo* _info);

	void OnFileInfoOutput(SCommandInfo* _filterinfo, CfileInfo* _info);
	void OnDirFileInfoOutputStart (SCommandInfo *_filterinfo, SFolderInfo* _info);
	void OnDirFileInfoOutputEnd (SCommandInfo *_filterinfo, SFolderInfo* _info);
	void OnFileInfoPrint (SCommandInfo *_filterinfo, CfileInfo* _info);
};