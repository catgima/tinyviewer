# TinyViewer

macos asan need package and setting:
------------------------------------
brew install llvm@8

# Overwritten default Clang
$ echo 'export PATH="/usr/local/opt/llvm/bin:$PATH"' >> .zshrc

$ source ~/.zshrc
$ which clang
/usr/local/opt/llvm/bin/clang

To use the bundled libc++ please add the following LDFLAGS:
  LDFLAGS="-L/usr/local/opt/llvm@8/lib -Wl,-rpath,/usr/local/opt/llvm@8/lib"

llvm@8 is keg-only, which means it was not symlinked into /usr/local,
because this is an alternate version of another formula.

If you need to have llvm@8 first in your PATH run:
  echo 'export PATH="/usr/local/opt/llvm@8/bin:$PATH"' >> ~/.bash_profile
  echo 'export ASAN_OPTIONS=detect_leaks=1' >> ~/.bash_profile

For compilers to find llvm@8 you may need to set:
  export LDFLAGS="-L/usr/local/opt/llvm@8/lib"
  export CPPFLAGS="-I/usr/local/opt/llvm@8/include"