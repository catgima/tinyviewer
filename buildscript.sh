#!/bin/bash

function build_exec()
{
	OUTPUT_DIR=""
	MakeSelect="make"

	echo "OSTYPE=${OSTYPE}"
	
	if [[ "$OSTYPE" == "cygwin" ]]; then
		OUTPUT_DIR="./linuxbuild"

	elif [[ "$OSTYPE" == "msys" ]]; then
		MakeSelect="mingw32-make"
		OUTPUT_DIR="./mingwbuild"

	elif [[ "$OSTYPE" == "win32" ]]; then
		MakeSelect="mingw32-make"
		OUTPUT_DIR="./mingwbuild"
		
	elif [[ "$OSTYPE" == "linux-gnu" ]]; then
		OUTPUT_DIR="./linuxbuild"
		export LDFLAGS="-L/usr/lib/llvm-10/lib"
  		export CPPFLAGS="-I/usr/lib/llvm-10/include"

	elif [[ "$OSTYPE" == "darwin"* ]]; then
		export PATH="/opt/homebrew/opt/openssl/bin:$PATH"
		OUTPUT_DIR="./osxbuild"
	
	elif [[ "$OSTYPE" == "freebsd"* ]]; then
		OUTPUT_DIR="./bsdbuild"
	else
		echo "build command parameter has error!!!"
		return
	fi

	if [ "${PLATFORM}" == "mingw" ]; then
		OUTPUT_DIR="./mingwbuild"
	fi

	if [ -d "$OUTPUT_DIR" ]; then
		cd $OUTPUT_DIR
		$MakeSelect
		cd ..
	else
		echo "$OUTPUT_DIR not found, need run cmake update"
	fi
}

function cmake_exec()
{
	TOOL_CHAIN=""
	OUTPUT_DIR=""

	echo "OSTYPE=${OSTYPE}"
	
	if [[ "$OSTYPE" == "cygwin" ]]; then
		OUTPUT_DIR="./linuxbuild"

	elif [[ "$OSTYPE" == "msys" ]]; then
		OUTPUT_DIR="./mingwbuild"

	elif [[ "$OSTYPE" == "win32" ]]; then
		TOOL_CHAIN="./cmake_scripts/MingW.cmake"
		OUTPUT_DIR="./mingwbuild"
		
	elif [[ "$OSTYPE" == "linux-gnu" ]]; then
		OUTPUT_DIR="./linuxbuild"
		#export PATH="/usr/lib/llvm-10/bin:$PATH"
		#export LDFLAGS="-L/usr/lib/llvm-10/lib"
  		#export CPPFLAGS="-I/usr/lib/llvm-10/include"
		#export ASAN_OPTIONS=detect_leaks=1
		TOOL_CHAIN="-DCMAKE_C_COMPILER=$(which gcc) -DCMAKE_CXX_COMPILER=$(which g++)"

	elif [[ "$OSTYPE" == "darwin"* ]]; then
		OUTPUT_DIR="./osxbuild"
		ulimit -v
		export ASAN_OPTIONS=detect_leaks=1
		TOOL_CHAIN="-DCMAKE_C_COMPILER=$(which clang) -DCMAKE_CXX_COMPILER=$(which clang++) -DCMAKE_OSX_SYSROOT=/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk"
				
	elif [[ "$OSTYPE" == "freebsd"* ]]; then
		OUTPUT_DIR="./bsdbuild"
	else
		echo "build command parameter has error!!!"
		return
	fi

	if [ "${PLATFORM}" == "mingw" ]; then
		TOOL_CHAIN=-DCMAKE_TOOLCHAIN_FILE="./cmake_scripts/MingW.cmake"
		OUTPUT_DIR="./mingwbuild"
	fi
 
	#if [ -d "$OUTPUT_DIR" ]; then
	#	echo "Remove old directory: ${OUTPUT_DIR}"
	#	rm -rf ${OUTPUT_DIR}
	#fi

	EXEC_COMMAND="${BUILD_TYPE} -H./ ${TOOL_CHAIN} -B${OUTPUT_DIR}"
	echo "exec cmake $EXEC_COMMAND"
	cmake $EXEC_COMMAND
}

#------------------------------------------------

COMMAND_LINE=""
MAKE_CMD1=""
MAKE_CMD2=""
BUILD_TYPE=""
PLATFORM=""

while [[ $# > 0 ]]
do
case "$1" in
	mingw)	
		PLATFORM="mingw"	
		shift
	;;
	default)	
		PLATFORM="default"	
		shift
	;;
    build)	
		MAKE_CMD2="make"	
		shift
	;;
	cmake)	
		MAKE_CMD1="cmake"	
		shift
	;;
	release)
		BUILD_TYPE="-DCMAKE_BUILD_TYPE=Release"
		shift
	;;
	debug)
		BUILD_TYPE="-DCMAKE_BUILD_TYPE=Debug"
		shift
	;;
    *)		
		COMMAND_LINE+=$1
		COMMAND_LINE+=" "
    	shift 
	;;
esac
done

#------------------------------------------------
clear

if [ "${MAKE_CMD1}" == "cmake" ]; then
	cmake_exec
fi

if [ "${MAKE_CMD2}" == "make" ]; then
	build_exec
fi




