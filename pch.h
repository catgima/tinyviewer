#if defined(_MSC_VER) && (_MSC_VER >= 1900)    
	# pragma execution_character_set("utf-8")    
#endif

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <unordered_map>
#include <functional>
#include <codecvt>
#include <locale>
#include <regex>

#if defined(__MINGW32__)
	#include <windows.h>
#else	
	#include <unistd.h>
#endif

/*
#include "wx/wxprec.h"
#include "wx/aui/aui.h"
#include <wx/listctrl.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include "wx/grid.h"
#include "wx/menu.h"
#include "wx/timer.h"
#include "wx/combo.h"
#include "wx/event.h"
#include "wx/treectrl.h"
*/

#include "objectbase.h"
#include "customdefine.h"


